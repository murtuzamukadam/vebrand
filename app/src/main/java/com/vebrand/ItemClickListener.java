package com.vebrand;

import android.util.Log;

public interface ItemClickListener {


    default void onDirectionClicked(int position){
        System.out.print("closing ... ");
    }

    default void onCallOwnerClicked(int position){
        System.out.print("closing ... ");
    }

    default void onDoneClicked(int position){
        System.out.print("closing ... ");
    }

     void onItemClick(int position);
}
