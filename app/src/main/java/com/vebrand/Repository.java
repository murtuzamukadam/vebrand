package com.vebrand;

import android.app.Application;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.core.util.Pair;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.api.Api;
import com.vebrand.database.AppDatabase;
import com.vebrand.model.ClientResponse;
import com.vebrand.model.Clients;
import com.vebrand.model.DisplayForm;
import com.vebrand.model.DisplayLocalForm;
import com.vebrand.model.Form;
import com.vebrand.model.FormResponse;
import com.vebrand.model.FormsWithStatus;
import com.vebrand.model.InsertFormResponse;
import com.vebrand.model.JobResponse;
import com.vebrand.model.JobStoreResponse;
import com.vebrand.model.JobStores;
import com.vebrand.model.JobStoresAndStores;
import com.vebrand.model.Jobs;
import com.vebrand.model.LocalForm;
import com.vebrand.model.LocalFormWithStatus;
import com.vebrand.model.MastersResponse;
import com.vebrand.model.Product;
import com.vebrand.model.ProductResponse;
import com.vebrand.model.ResponseData;
import com.vebrand.model.Statuses;
import com.vebrand.model.Store;
import com.vebrand.model.Unit;
import com.vebrand.model.User;
import com.vebrand.utils.BackgroundExecutor;
import com.vebrand.utils.BackgroundExecutor.CallBack;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.RESULT;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {

    private static final String TAG = Repository.class.getSimpleName();
    Application application;
    static Repository repository;
    AppDatabase appDatabase;
    static ExecutorService executorService = Executors.newCachedThreadPool();

    private Repository(Application application) {
        this.application = application;
    }

    public static Repository repositoryInstance(Application application) {

        if (repository == null) {
            repository = new Repository(application);
        }
        return repository;


    }

    public void copyFileUri(Uri inputUri, String nonEditedPath, int imageNumber, CallBack<Integer> callBack) {
        Callable<Integer> callable = () -> {
            Utils.copyFileUri(application, inputUri, nonEditedPath);
            return imageNumber;
        };
        new BackgroundExecutor().executeAsync(callable, callBack, executorService);

    }

    //Login procedure happens in the below methods. Also note @checkLogin is called within tokenAuthenticator as well to
    //get new tokens
    public void requestOTP(String id, MutableLiveData<User> requestOTPResponse, MutableLiveData<Pair<RESULT, String>> checkErrors) {

        Call<ResponseData> requestOTP = Api.getInstance(application).requestOTP(id);

        requestOTP.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {

                    requestOTPResponse.setValue(response.body().getUser());
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });
    }

    public void checkLogin(String login, String password, String type, MutableLiveData<User> checkLoginResponse, MutableLiveData<Pair<RESULT, String>> checkErrors) {

        Call<ResponseData> checkLogin = Api.getInstance(application).login(login, password, type);

        checkLogin.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    checkLoginResponse.setValue(response.body().getUser());
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG).replace("OTP", "Password");
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });
    }

    public void insertNewUser(User user, CallBack<Long> callBack) {
        Callable<Long> callable = () -> getDatabase().appDao().deleteAndInsert(user);

        new BackgroundExecutor().executeAsync(callable, callBack, executorService);
    }

    public void getUser(CallBack<User> callBack) {
        Callable<User> callable = () -> getDatabase().appDao().getUser();

        new BackgroundExecutor().executeAsync(callable, callBack, executorService);
    }

    //Clients apicall and database insertion happens below
    public void getClients(MutableLiveData<List<Clients>> clientsList, MutableLiveData<Pair<RESULT, String>> checkErrors) {

        String token = Utils.getBearerToken(application);
        Call<ClientResponse> test = Api.getInstance(application).getClients(token, "[]", "[[\"id\",\"desc\"]]", "0", "500", "");

        test.enqueue(new Callback<ClientResponse>() {
            @Override
            public void onResponse(Call<ClientResponse> call, Response<ClientResponse> response) {
                if (response.isSuccessful()) {
                    clientsList.setValue(response.body().getClients());
                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ClientResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void insertClientList(List<Clients> clients, CallBack<List<Long>> callback) {
        Callable<List<Long>> callable = () -> getDatabase().appDao().insertClientList(clients);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getClientListFromDatabase(CallBack<List<Clients>> callback) {
        Callable<List<Clients>> callable = () -> getDatabase().appDao().getClientList();

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    //Jobs apicall and database insertion happens below
    public void getJobs(MutableLiveData<List<Jobs>> jobsList, MutableLiveData<Pair<RESULT, String>> checkErrors, String clientId) {

        String token = Utils.getBearerToken(application);
        String filter = "[[\"filter_type\",\"recce-team-job\"],[\"client_id\",\"=\",\"" + clientId + "\"]]";
        Log.d(TAG, "getJobs: filter " + filter);
        Call<JobResponse> test = Api.getInstance(application).getJobs(token, filter, "[[\"id\",\"desc\"]]", "0", "500", "");

        test.enqueue(new Callback<JobResponse>() {
            @Override
            public void onResponse(Call<JobResponse> call, Response<JobResponse> response) {
                if (response.isSuccessful()) {
                    jobsList.setValue(response.body().getJobs());
                    Log.d(TAG, "onResponse: jobs " + response.body().getJobs().get(0).getClientId());
                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void insertJobList(List<Jobs> jobs, CallBack<List<Long>> callback) {
        Callable<List<Long>> callable = () -> getDatabase().appDao().insertJobList(jobs);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getJobListFromDatabase(int clientId, CallBack<List<Jobs>> callback) {
        Callable<List<Jobs>> callable = () -> getDatabase().appDao().getJobList(clientId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    //JobStores apicall and database insertion happens below
    public void getJobStores(MutableLiveData<List<JobStores>> jobStoreList, MutableLiveData<Pair<RESULT, String>> checkErrors, String jobId) {

        String token = Utils.getBearerToken(application);
        String filter = "[[\"filter_type\",\"recce\"],[\"job_id\",\"=\",\"" + jobId + "\"]]";
        Log.d(TAG, "getJobStores: filter " + filter);
        Call<JobStoreResponse> test = Api.getInstance(application).getJobStores(token, filter, "[[\"id\",\"desc\"]]", "0", "500", "");

        test.enqueue(new Callback<JobStoreResponse>() {
            @Override
            public void onResponse(Call<JobStoreResponse> call, Response<JobStoreResponse> response) {
                if (response.isSuccessful()) {
                    jobStoreList.setValue(response.body().getJobStores());
                    Log.d(TAG, "onResponse: jobs " + response.body().getJobStores().get(0).getClientId());
                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobStoreResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void insertStore(Store store) {
        getDatabase().appDao().insertStore(store);
    }

    public void insertJobStoreList(List<JobStores> jobStores, CallBack<List<Long>> callback) {
        Callable<List<Long>> callable = () -> getDatabase().appDao().insertJobStoresList(jobStores);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getJobStoresListFromDatabase(int clientId, int jobId, CallBack<List<JobStores>> callback) {
        Callable<List<JobStores>> callable = () -> getDatabase().appDao().getJobStoresList(clientId, jobId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getJobStoresAndStoresListFromDatabase(int clientId, int jobId, CallBack<List<JobStoresAndStores>> callback) {
        Callable<List<JobStoresAndStores>> callable = () -> getDatabase().appDao().getJobStoresAndStores(clientId, jobId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    //FormsArrayList apicall and database insertion happens below
    public void getForms(MutableLiveData<List<Form>> formsList, MutableLiveData<Pair<RESULT, String>> checkErrors, String jobStoreId) {

        String token = Utils.getBearerToken(application);
        String filter = "[[\"filter_type\",\"recce\"],[\"job_store_id\",\"=\",\"" + jobStoreId + "\"]]";
        Log.d(TAG, "getForms: filter " + filter);
        Call<FormResponse> test = Api.getInstance(application).getForms(token, filter, "[[\"id\",\"desc\"]]", "0", "500", "");

        test.enqueue(new Callback<FormResponse>() {
            @Override
            public void onResponse(Call<FormResponse> call, Response<FormResponse> response) {
                if (response.isSuccessful()) {
                    formsList.setValue(response.body().getForms());
                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FormResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void insertFormList(List<Form> forms, CallBack<List<Long>> callback) {
        Callable<List<Long>> callable = () -> getDatabase().appDao().insertFormList(forms);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getFormListFromDatabase(int clientId, int jobId, int jobStoreId, int storeId, CallBack<List<Form>> callback) {
        Callable<List<Form>> callable = () -> getDatabase().appDao().getFormList(clientId, jobId, jobStoreId, storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getFormListStatusesFromDatabase(int clientId, int jobId, int jobStoreId, int storeId, CallBack<List<FormsWithStatus>> callback) {
        Callable<List<FormsWithStatus>> callable = () -> getDatabase().appDao().getFormListStatuses(clientId, jobId, jobStoreId, storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getLocalFormListStatusesFromDatabase(int clientId, int jobId, int jobStoreId, int storeId, CallBack<List<LocalFormWithStatus>> callback) {
        Callable<List<LocalFormWithStatus>> callable = () -> getDatabase().appDao().getLocalFormListStatuses(clientId, jobId, jobStoreId, storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    //gettingFormToDisplay
    public void getDisplayFormFromDatabase(int formId, int clientId, int jobId, int jobStoreId, int storeId, CallBack<DisplayForm> callback) {
        Callable<DisplayForm> callable = () -> getDatabase().appDao().getFormDisplay(formId, clientId, jobId, jobStoreId, storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getDisplayLocalFormFromDatabase(int formId, int clientId, int jobId, int jobStoreId, int storeId, CallBack<DisplayLocalForm> callback) {
        Callable<DisplayLocalForm> callable = () -> getDatabase().appDao().getLocalFormDisplay(formId, clientId, jobId, jobStoreId, storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getAllUnitsFromDatabase(CallBack<List<Unit>> callback) {
        Callable<List<Unit>> callable = () -> getDatabase().appDao().getAllUnits();

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    //getting storename
    public void getStoreNameFromDatabase(int storeId, CallBack<String> callback) {
        Callable<String> callable = () -> getDatabase().appDao().getStoreName(storeId);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getStatusesFromDatabase(CallBack<List<Statuses>> callback) {
        Callable<List<Statuses>> callable = () -> getDatabase().appDao().getAllStatuses();

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getStatusIdFromDatabase(String statusName, CallBack<Integer> callback) {
        Callable<Integer> callable = () -> getDatabase().appDao().getStatusId(statusName);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void insertLocalForm(LocalForm localForm, CallBack<Long> callback) {
        Callable<Long> callable = () -> getDatabase().appDao().insertLocalForm(localForm);

        new BackgroundExecutor().executeAsync(callable, callback, executorService);
    }

    public void getMasters() {

        String models = "unit,job_status";
        Call<MastersResponse> test = Api.getInstance(application).getMasters(models);

        test.enqueue(new Callback<MastersResponse>() {
            @Override
            public void onResponse(Call<MastersResponse> call, Response<MastersResponse> response) {
                if (response.isSuccessful()) {
                    List<Unit> units = response.body().getMasterResponses().getUnit();

                    List<Statuses> statuses = response.body().getMasterResponses().getStatuses();

                    insertMasters(units, statuses);
                }
            }

            @Override
            public void onFailure(Call<MastersResponse> call, Throwable t) {

            }
        });
    }

    void insertMasters(List<Unit> units, List<Statuses> statuses) {

        getDatabase().appDao().insertUnitList(units);

        getDatabase().appDao().insertStatusesList(statuses);
    }

    public void getProducts(String jobStoreId, MutableLiveData<List<Product>> productList) {

        Call<ProductResponse> test = Api.getInstance(application).getProducts(jobStoreId);

        test.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(() -> productList.setValue(response.body().getProducts()));
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {

            }
        });
    }

    public void insertFormApi(RequestBody id, RequestBody jobStoreId, RequestBody productId, RequestBody width, RequestBody widthUnitId,
                              RequestBody height, RequestBody heightUnitId, RequestBody quantity, RequestBody recce_remark, RequestBody type,
                              MultipartBody.Part recceImage, MultipartBody.Part recceImageEdited, MultipartBody.Part recce1Image, MultipartBody.Part recce1ImageEdited,
                              MultipartBody.Part recce2Image, MultipartBody.Part recce2ImageEdited, MultipartBody.Part recce3Image, MultipartBody.Part recce3ImageEdited,
                              MultipartBody.Part recce4Image, MultipartBody.Part recce4ImageEdited, MultipartBody.Part recceCertificate, MutableLiveData<Pair<RESULT, String>> checkErrors,
                              MutableLiveData<Boolean> isInsertionSuccessful, int formId, int jobStoreIdId) {
        String token = Utils.getBearerToken(application);

        Call<InsertFormResponse> test = Api.getInstance(application).insertForm(token,id, jobStoreId, productId, width, widthUnitId, height, heightUnitId,
                quantity, recce_remark, type, recceImage, recceImageEdited, recce1Image, recce1ImageEdited, recce2Image, recce2ImageEdited, recce3Image, recce3ImageEdited, recce4Image, recce4ImageEdited, recceCertificate);

        test.enqueue(new Callback<InsertFormResponse>() {
            @Override
            public void onResponse(Call<InsertFormResponse> call, Response<InsertFormResponse> response) {
                if (response.isSuccessful()) {
                    getDatabase().appDao().deleteLocalForm(formId, jobStoreIdId);
                    new Handler(Looper.getMainLooper()).post(() -> isInsertionSuccessful.setValue(true));

                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    new Handler(Looper.getMainLooper()).post(() -> isInsertionSuccessful.setValue(false));
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
                    }
                }

            }

            @Override
            public void onFailure(Call<InsertFormResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void updateFormApi(RequestBody id, RequestBody jobStoreId, RequestBody productId, RequestBody width, RequestBody widthUnitId,
                              RequestBody height, RequestBody heightUnitId, RequestBody quantity, RequestBody recce_remark, RequestBody type,
                              MultipartBody.Part recceImage, MultipartBody.Part recceImageEdited, MultipartBody.Part recce1Image, MultipartBody.Part recce1ImageEdited,
                              MultipartBody.Part recce2Image, MultipartBody.Part recce2ImageEdited, MultipartBody.Part recce3Image, MultipartBody.Part recce3ImageEdited,
                              MultipartBody.Part recce4Image, MultipartBody.Part recce4ImageEdited, MultipartBody.Part recceCertificate, MutableLiveData<Pair<RESULT, String>> checkErrors,
                              MutableLiveData<Boolean> isUpdateSuccessful) {
        String token = Utils.getBearerToken(application);

        Call<InsertFormResponse> test = Api.getInstance(application).updateForm(token,id, jobStoreId, productId, width, widthUnitId, height, heightUnitId,
                quantity, recce_remark, type, recceImage, recceImageEdited, recce1Image, recce1ImageEdited, recce2Image, recce2ImageEdited, recce3Image, recce3ImageEdited, recce4Image, recce4ImageEdited, recceCertificate);

        test.enqueue(new Callback<InsertFormResponse>() {
            @Override
            public void onResponse(Call<InsertFormResponse> call, Response<InsertFormResponse> response) {
                if (response.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(() -> isUpdateSuccessful.setValue(true));

                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    new Handler(Looper.getMainLooper()).post(() -> isUpdateSuccessful.setValue(false));
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
                    }
                }

            }

            @Override
            public void onFailure(Call<InsertFormResponse> call, Throwable t) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });

    }

    public void updateFormStatus(String formId, String statusId, String type,MutableLiveData<Boolean> isUpdateSuccessful,MutableLiveData<Pair<RESULT, String>> checkErrors){
        String token = Utils.getBearerToken(application);
        Call<Void> test = Api.getInstance(application).updateFormStatus(token,formId,statusId,type);
        test.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(() -> isUpdateSuccessful.setValue(true));

                } else if (response.code() == 401) {
                    Log.d(TAG, "onResponse: error code 401 new token is being generated");
                    //intentionally left empty so an error message is not thrown. @TokenAuthenticator generates a ne token in this event
                } else {
                    new Handler(Looper.getMainLooper()).post(() -> isUpdateSuccessful.setValue(false));
                    try {
                        String finalMessage = Utils.convertErrorMessage(response.errorBody().string(), TAG);
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.UNSUCCESSFUL, finalMessage)));
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
                    }
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, t.toString())));
            }
        });
    }


    public AppDatabase getDatabase() {
        if (appDatabase == null) {
            appDatabase = AppDatabase.getDatabase(application);
        }
        return appDatabase;
    }

}
