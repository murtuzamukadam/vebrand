package com.vebrand.api;

import com.vebrand.model.ClientResponse;
import com.vebrand.model.FormResponse;
import com.vebrand.model.InsertFormResponse;
import com.vebrand.model.JobResponse;
import com.vebrand.model.JobStoreResponse;
import com.vebrand.model.MastersResponse;
import com.vebrand.model.ProductResponse;
import com.vebrand.model.ResponseData;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIRequests {

    @POST("users/request-otp")
    @FormUrlEncoded
    Call<ResponseData> requestOTP(@Field("login") String id);

    @POST("users/login")
    @FormUrlEncoded
    Call<ResponseData> login(@Field("login") String login,
                             @Field("otp") String otp,
                             @Field("type") String type);


    @GET("team/clients")
    Call<ClientResponse> getClients(@Header("Authorization") String accessToken,
                                    @Query("filters") String filters,
                                    @Query("orderBy") String orderBy,
                                    @Query("pageNumber") String pageNumber,
                                    @Query("pageSize") String pageSize,
                                    @Query("masters") String masters);

    @GET("job")
    Call<JobResponse> getJobs(@Header("Authorization") String accessToken,
                              @Query("filters") String filters,
                              @Query("orderBy") String orderBy,
                              @Query("pageNumber") String pageNumber,
                              @Query("pageSize") String pageSize,
                              @Query("masters") String masters);

    @GET("job_store")
    Call<JobStoreResponse> getJobStores(@Header("Authorization") String accessToken,
                                        @Query("filters") String filters,
                                        @Query("orderBy") String orderBy,
                                        @Query("pageNumber") String pageNumber,
                                        @Query("pageSize") String pageSize,
                                        @Query("masters") String masters);

    @GET("job_store_form")
    Call<FormResponse> getForms(@Header("Authorization") String accessToken,
                                @Query("filters") String filters,
                                @Query("orderBy") String orderBy,
                                @Query("pageNumber") String pageNumber,
                                @Query("pageSize") String pageSize,
                                @Query("masters") String masters);

    @GET("masters")
    Call<MastersResponse> getMasters(@Query("models") String models);

    @GET("job_store_form/0")
    Call<ProductResponse> getProducts(@Query("job_store_id") String jobStoreId);

    @POST("job_store_form")
    @Multipart
    Call<InsertFormResponse> insertForm(@Header("Authorization") String accessToken,
                                        @Part("id") RequestBody id,
                                        @Part("job_store_id") RequestBody jobStoreId,
                                        @Part("product_id") RequestBody productId,
                                        @Part("width") RequestBody width,
                                        @Part("width_unit_id") RequestBody widthUnitId,
                                        @Part("height") RequestBody height,
                                        @Part("height_unit_id") RequestBody heightUnitId,
                                        @Part("quantity") RequestBody quantity,
                                        @Part("recce_remark") RequestBody recce_remark,
                                        @Part("type") RequestBody type,
                                        @Part MultipartBody.Part recceImage,
                                        @Part MultipartBody.Part recceImageEdited,
                                        @Part MultipartBody.Part recce1Image,
                                        @Part MultipartBody.Part recce1ImageEdited,
                                        @Part MultipartBody.Part recce2Image,
                                        @Part MultipartBody.Part recce2ImageEdited,
                                        @Part MultipartBody.Part recce3Image,
                                        @Part MultipartBody.Part recce3ImageEdited,
                                        @Part MultipartBody.Part recce4Image,
                                        @Part MultipartBody.Part recce4ImageEdited,
                                        @Part MultipartBody.Part recceCertificate);

    @PATCH("job_store_form")
    @Multipart
    Call<InsertFormResponse> updateForm(@Header("Authorization") String accessToken,
                                        @Part("id") RequestBody id,
                                        @Part("job_store_id") RequestBody jobStoreId,
                                        @Part("product_id") RequestBody productId,
                                        @Part("width") RequestBody width,
                                        @Part("width_unit_id") RequestBody widthUnitId,
                                        @Part("height") RequestBody height,
                                        @Part("height_unit_id") RequestBody heightUnitId,
                                        @Part("quantity") RequestBody quantity,
                                        @Part("recce_remark") RequestBody recce_remark,
                                        @Part("type") RequestBody type,
                                        @Part MultipartBody.Part recceImage,
                                        @Part MultipartBody.Part recceImageEdited,
                                        @Part MultipartBody.Part recce1Image,
                                        @Part MultipartBody.Part recce1ImageEdited,
                                        @Part MultipartBody.Part recce2Image,
                                        @Part MultipartBody.Part recce2ImageEdited,
                                        @Part MultipartBody.Part recce3Image,
                                        @Part MultipartBody.Part recce3ImageEdited,
                                        @Part MultipartBody.Part recce4Image,
                                        @Part MultipartBody.Part recce4ImageEdited,
                                        @Part MultipartBody.Part recceCertificate);

    @PATCH("job_store_form/jobStatus")
    @FormUrlEncoded
    Call<Void> updateFormStatus(@Header("Authorization") String accessToken,
                                @Field("id") String formId,
                                @Field("job_status_id") String statusId,
                                @Field("type") String type);


}
