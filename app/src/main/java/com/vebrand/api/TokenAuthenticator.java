package com.vebrand.api;

import android.app.Application;

import com.vebrand.Repository;
import com.vebrand.model.ResponseData;
import com.vebrand.model.User;
import com.vebrand.utils.Utils;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Call;

public class TokenAuthenticator implements Authenticator {

    Application application;

    public TokenAuthenticator(Application application){
        this.application = application;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        User originalUSer = Repository.repositoryInstance(application).getDatabase().appDao().getUser();
        Call<ResponseData> checkLogin = Api.getInstance(application).login(originalUSer.getLogin(), originalUSer.getPassword(), "login");

        User updatedUserWithNewToken = checkLogin.execute().body().getUser();

        if(updatedUserWithNewToken.getToken() != null && !updatedUserWithNewToken.getToken().equalsIgnoreCase("")){
            Repository.repositoryInstance(application).getDatabase().appDao().updateToken(updatedUserWithNewToken.getToken(),originalUSer.getUserId());
            Utils.insertToken(application,updatedUserWithNewToken.getToken());
        }

        return response.request().newBuilder()
                .header("Authorization", Utils.getBearerToken(application))
                .build();
    }
}
