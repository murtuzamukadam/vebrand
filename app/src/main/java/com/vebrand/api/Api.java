package com.vebrand.api;

import static com.vebrand.utils.Utils.BASE_URL;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {


    private static APIRequests apiRequests;

    public static APIRequests getInstance(Application application) {

        if (apiRequests == null) {

            Gson gson = new GsonBuilder()
                    .setLenient().excludeFieldsWithoutExposeAnnotation()
                    .create();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            TokenAuthenticator tokenAuthenticator = new TokenAuthenticator(application);

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(logging).authenticator(tokenAuthenticator)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();


            apiRequests = retrofit.create(APIRequests.class);

            return apiRequests;
        } else {
            return apiRequests;
        }
    }
}
