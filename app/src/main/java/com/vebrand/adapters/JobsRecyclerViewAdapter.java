package com.vebrand.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.R;
import com.vebrand.model.Jobs;

import java.util.List;

public class JobsRecyclerViewAdapter extends ListAdapter<Jobs, JobsRecyclerViewAdapter.JobsViewHolder> {

    ItemClickListener itemClickListener;

    List<Jobs> jobsList;

    public static final DiffUtil.ItemCallback<Jobs> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Jobs>() {
                @Override
                public boolean areItemsTheSame(@NonNull Jobs oldItem, @NonNull Jobs newItem) {
                    return oldItem.getJobId() == newItem.getJobId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Jobs oldItem, @NonNull Jobs newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public JobsRecyclerViewAdapter(ItemClickListener itemClickListener, List<Jobs> jobs) {
        super(DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
        this.jobsList = jobs;
    }


    @NonNull
    @Override
    public JobsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_jobs, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull JobsViewHolder holder, int position) {
        holder.jobId.setText(String.valueOf(jobsList.get(position).getJobId()));
        //holder.jobId.setText("4556456");
        holder.jobName.setText(jobsList.get(position).getJobName());
        holder.jobDescription.setText(jobsList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return jobsList.size();
    }

    @Override
    public long getItemId(int position) {
        return jobsList.get(position).getJobId();
    }

    class JobsViewHolder extends RecyclerView.ViewHolder {

        TextView jobId, jobName, jobDescription;

        public JobsViewHolder(@NonNull View itemView) {
            super(itemView);

            jobId = itemView.findViewById(R.id.text_job_number_recyclerview_jobs);
            jobName = itemView.findViewById(R.id.text_job_name_recyclerview_jobs);
            jobDescription = itemView.findViewById(R.id.text_job_description_recyclerview_jobs);


            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) itemClickListener.onItemClick(getAdapterPosition());
            });
        }
    }
}
