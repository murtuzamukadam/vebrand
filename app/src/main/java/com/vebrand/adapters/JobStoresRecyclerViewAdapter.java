package com.vebrand.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.R;
import com.vebrand.model.JobStoresAndStores;

import java.util.List;

public class JobStoresRecyclerViewAdapter extends ListAdapter<JobStoresAndStores, JobStoresRecyclerViewAdapter.JobStoresViewHolder> {

    ItemClickListener itemClickListener;

    List<JobStoresAndStores> jobStoresAndStoresList;

    public static final DiffUtil.ItemCallback<JobStoresAndStores> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<JobStoresAndStores>() {
                @Override
                public boolean areItemsTheSame(@NonNull JobStoresAndStores oldItem, @NonNull JobStoresAndStores newItem) {
                    return oldItem.getJobStores().getJobStoresId() == newItem.getJobStores().getJobStoresId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull JobStoresAndStores oldItem, @NonNull JobStoresAndStores newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public JobStoresRecyclerViewAdapter(ItemClickListener itemClickListener, List<JobStoresAndStores> jobStoresAndStoresList) {
        super(DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
        this.jobStoresAndStoresList = jobStoresAndStoresList;
    }


    @NonNull
    @Override
    public JobStoresViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobStoresViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_job_store, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull JobStoresViewHolder holder, int position) {
        JobStoresAndStores jobStoresAndStores = jobStoresAndStoresList.get(position);
        String id = String.valueOf(jobStoresAndStores.getJobStores().getJobStoresId());
        String storeName = jobStoresAndStores.getStore().getName();
        String area = jobStoresAndStores.getStore().getArea();
        String address = jobStoresAndStores.getStore().getAddress();
        String contact = jobStoresAndStores.getStore().getMobile();
        String clientName = jobStoresAndStores.getClients().getClientName();

        holder.id.setText(id);
        holder.storeName.setText(storeName);
        holder.area.setText(area);
        holder.address.setText(address);
        holder.contact.setText(contact);
        holder.clientName.setText(clientName);
    }

    public void setFilter(List<JobStoresAndStores> FilteredDataList) {
        jobStoresAndStoresList = FilteredDataList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return jobStoresAndStoresList.size();
    }

    @Override
    public long getItemId(int position) {
        return jobStoresAndStoresList.get(position).getJobStores().getJobStoresId();
    }

    class JobStoresViewHolder extends RecyclerView.ViewHolder {

        TextView id, storeName, area, address, contact, clientName,callText,directionText;
        ImageView call,map;
        Button doneButton;
        Group callGroup;

        public JobStoresViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.text_job_store_id_recyclerview_job_store);
            storeName = itemView.findViewById(R.id.text_store_name_recyclerview_job_store);
            address = itemView.findViewById(R.id.text_address_name_recyclerview_job_store);
            area = itemView.findViewById(R.id.text_area_name_recyclerview_job_store);
            contact = itemView.findViewById(R.id.text_mobile_recyclerview_job_store);
            clientName = itemView.findViewById(R.id.text_client_name_recyclerview_job_store);
            callText = itemView.findViewById(R.id.text_call_owner_recyclerview_job_store);
            directionText = itemView.findViewById(R.id.text_direction_recyclerview_job_store);
            call = itemView.findViewById(R.id.image_call_owner_recyclerview_job_store);
            map = itemView.findViewById(R.id.image_direction_recyclerview_job_store);

            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) itemClickListener.onItemClick(getAdapterPosition());
            });
            doneButton = itemView.findViewById(R.id.button_done_recyclerview_job_store);
            doneButton.setVisibility(View.GONE);
            callGroup = itemView.findViewById(R.id.group_call);

            call.setOnClickListener(v -> itemClickListener.onCallOwnerClicked(getAdapterPosition()));

            map.setOnClickListener(v -> itemClickListener.onDirectionClicked(getAdapterPosition()));

            callText.setOnClickListener(v -> itemClickListener.onCallOwnerClicked(getAdapterPosition()));

            directionText.setOnClickListener(v -> itemClickListener.onDirectionClicked(getAdapterPosition()));

        }
    }
}
