package com.vebrand.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.R;
import com.vebrand.model.FormsWithStatus;
import com.vebrand.model.LocalFormWithStatus;
import com.vebrand.viewmodel.FormViewModel;

import java.util.List;

public class FormRecyclerViewAdapter extends ListAdapter<FormsWithStatus, FormRecyclerViewAdapter.FormViewHolder> {
    ItemClickListener itemClickListener;
    List<FormsWithStatus> formList;
    List<LocalFormWithStatus> localForms;

    public static final DiffUtil.ItemCallback<FormsWithStatus> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<FormsWithStatus>() {


                @Override
                public boolean areItemsTheSame(@NonNull FormsWithStatus oldItem, @NonNull FormsWithStatus newItem) {
                    return oldItem.getForm().getFormId() == newItem.getForm().getFormId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull FormsWithStatus oldItem, @NonNull FormsWithStatus newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public FormRecyclerViewAdapter(ItemClickListener itemClickListener, List<FormsWithStatus> formList, List<LocalFormWithStatus> localForms) {
        super(DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
        this.formList = formList;
        this.localForms = localForms;
    }

    @NonNull
    @Override
    public FormViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FormViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_form, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FormViewHolder holder, int position) {
        if(position >= localForms.size()) {
            holder.uploadButton.setVisibility(View.GONE);
            int pos = position - localForms.size();
            FormsWithStatus forms = formList.get(pos);
            if (forms.getStatuses().getName().equalsIgnoreCase("pending") || forms.getStatuses().getName().equalsIgnoreCase("revisit"))
                holder.doneButton.setVisibility(View.VISIBLE);
            else holder.doneButton.setVisibility(View.GONE);

            holder.recceStatus.setText(forms.getStatuses().getName());
            holder.formId.setText(String.valueOf(forms.getForm().getFormId()));
        } else {
            holder.uploadButton.setVisibility(View.VISIBLE);
            holder.doneButton.setVisibility(View.GONE);

            holder.recceStatus.setText(localForms.get(position).getStatuses().getName());
            holder.formId.setVisibility(View.GONE);
            //holder.formId.setText(String.valueOf(localForms.get(position).getLocalForm().getFormId()));
        }
    }

    @Override
    public int getItemCount() {
        int count = formList.size();
        if (localForms != null)
            count += localForms.size();
        return count;
    }

    @Override
    public long getItemId(int position) {
        if (position >= localForms.size()) {
            int pos = position - localForms.size();
            return formList.get(pos).getForm().getFormId();
        }
        return localForms.get(position).getLocalForm().getFormId();
    }

    class FormViewHolder extends RecyclerView.ViewHolder {

        TextView formId, recceStatus;

        ImageButton doneButton,uploadButton;

        public FormViewHolder(@NonNull View itemView) {
            super(itemView);

            formId = itemView.findViewById(R.id.text_form_number_recyclerview_form);
            recceStatus = itemView.findViewById(R.id.text_recce_status_recycler_form);
            doneButton = itemView.findViewById(R.id.button_done_recyclerview_form);
            uploadButton = itemView.findViewById(R.id.button_upload_recyclerview_form);

            itemView.setOnClickListener(v -> {
                if (itemClickListener != null) itemClickListener.onItemClick(getAdapterPosition());
            });

            doneButton.setOnClickListener(v -> {
                if (itemClickListener != null) itemClickListener.onDoneClicked(getAdapterPosition());
            });

            uploadButton.setOnClickListener(v -> {
                //Toast.makeText((Context) itemClickListener, "Done Clicked", Toast.LENGTH_SHORT).show();
                if (itemClickListener != null) itemClickListener.onItemClick(getAdapterPosition());
            });


        }
    }
}
