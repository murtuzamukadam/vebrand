package com.vebrand.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.R;
import com.vebrand.model.Clients;

import java.util.List;

public class ClientsRecyclerViewAdapter extends ListAdapter<Clients,ClientsRecyclerViewAdapter.ClientsViewHolder> {


    ItemClickListener itemClickListener;
    List<Clients> clientsList;

    public static final DiffUtil.ItemCallback<Clients> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Clients>() {


                @Override
                public boolean areItemsTheSame(@NonNull Clients oldItem, @NonNull Clients newItem) {
                    return oldItem.getClientId() == newItem.getClientId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull Clients oldItem, @NonNull Clients newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public ClientsRecyclerViewAdapter(ItemClickListener itemClickListener, List<Clients> clientsList){
        super(DIFF_CALLBACK);
        this.itemClickListener = itemClickListener;
        this.clientsList = clientsList;
    }

    @NonNull
    @Override
    public ClientsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_clients, parent, false);
        return new ClientsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientsViewHolder holder, int position) {
        holder.clientName.setText(clientsList.get(position).getClientName());

        String done = "D: " + clientsList.get(position).getDone();
        holder.done.setText(done);

        String pending = "P: " + clientsList.get(position).getPending();
        holder.pending.setText(pending);

        String total = "T: " + clientsList.get(position).getTotal();
        holder.total.setText(total);
    }

    @Override
    public long getItemId(int position) {
        return clientsList.get(position).getClientId();
    }

    @Override
    public int getItemCount() {
        return clientsList.size();
    }

    class ClientsViewHolder extends RecyclerView.ViewHolder{

        TextView clientName,pending,done,total;

        public ClientsViewHolder(@NonNull View itemView) {
            super(itemView);

            clientName = itemView.findViewById(R.id.text_client_name_recyclerview_clients);
            pending = itemView.findViewById(R.id.text_pending_number_recyclerview_clients);
            done = itemView.findViewById(R.id.text_done_number_recyclerview_clients);
            total = itemView.findViewById(R.id.text_total_number_recyclerview_clients);

            itemView.setOnClickListener(v -> {
                if(itemClickListener != null) itemClickListener.onItemClick(getAdapterPosition());
            });
        }
    }
}
