package com.vebrand.database;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.vebrand.model.Clients;
import com.vebrand.model.DisplayForm;
import com.vebrand.model.DisplayLocalForm;
import com.vebrand.model.Form;
import com.vebrand.model.FormsWithStatus;
import com.vebrand.model.JobStores;
import com.vebrand.model.JobStoresAndStores;
import com.vebrand.model.Jobs;
import com.vebrand.model.LocalForm;
import com.vebrand.model.LocalFormWithStatus;
import com.vebrand.model.Statuses;
import com.vebrand.model.Store;
import com.vebrand.model.Unit;
import com.vebrand.model.User;
import com.vebrand.utils.Utils;

import java.util.List;

@Dao
public abstract class AppDao {

    @Insert
    public abstract long insertNewUser(User user);

    @Query("DELETE FROM user")
    public abstract void deleteUserTableEntries();

    @Transaction
    public long deleteAndInsert(User user) {
        deleteUserTableEntries();
        return insertNewUser(user);
    }

    @Query("UPDATE user SET token = :token WHERE user_id = :userId")
    public abstract void updateToken(String token, String userId);

    @Query("SELECT * FROM " + Utils.USER_TABLE_NAME)
    public abstract User getUser();

    @Insert(onConflict = REPLACE)
    public abstract List<Long> insertClientList(List<Clients> clientsList);

    @Query("SELECT * FROM " + Utils.CLIENT_TABLE_NAME)
    public abstract List<Clients> getClientList();

    @Insert(onConflict = REPLACE)
    public abstract List<Long> insertJobList(List<Jobs> jobsList);

    @Query("SELECT * FROM " + Utils.JOBS_TABLE_NAME + " WHERE client_id = :clientId")
    public abstract List<Jobs> getJobList(int clientId);

    @Insert(onConflict = REPLACE)
    public abstract Long insertStore(Store store);

    @Query("SELECT * FROM " + Utils.STORE_TABLE_NAME + " WHERE store_id = :storeId")
    public abstract Store getStore(int storeId);

    @Query("SELECT store_name FROM " + Utils.STORE_TABLE_NAME + " WHERE store_id = :storeId")
    public abstract String getStoreName(int storeId);

    @Insert(onConflict = REPLACE)
    public abstract List<Long> insertJobStoresList(List<JobStores> jobStoresList);

    @Query("SELECT * FROM " + Utils.JOB_STORES_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId")
    public abstract List<JobStores> getJobStoresList(int clientId, int jobId);

    @Transaction
    @Query("SELECT * FROM " + Utils.JOB_STORES_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId")
    public abstract List<JobStoresAndStores> getJobStoresAndStores(int clientId, int jobId);

    @Insert(onConflict = REPLACE)
    public abstract List<Long> insertFormList(List<Form> formList);

    @Query("SELECT * FROM " + Utils.FORMS_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId AND job_store_id = :jobStoreId AND store_id = :storeId")
    public abstract List<Form> getFormList(int clientId, int jobId, int jobStoreId, int storeId);

    @Transaction
    @Query("SELECT * FROM " + Utils.FORMS_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId AND job_store_id = :jobStoreId AND store_id = :storeId")
    public abstract List<FormsWithStatus> getFormListStatuses(int clientId, int jobId, int jobStoreId, int storeId);

    @Transaction
    @Query("SELECT * FROM " + Utils.LOCAL_FORMS_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId AND job_store_id = :jobStoreId AND store_id = :storeId")
    public abstract List<LocalFormWithStatus> getLocalFormListStatuses(int clientId, int jobId, int jobStoreId, int storeId);

    @Query("SELECT * FROM " + Utils.FORMS_TABLE_NAME + " WHERE client_id = :clientId AND job_id = :jobId AND job_store_id = :jobStoreId AND store_id = :storeId AND form_id = :formId")
    public abstract Form getSingleForm(int clientId, int jobId, int jobStoreId, int storeId, int formId);

    @Insert(onConflict = REPLACE)
    public abstract void insertUnitList(List<Unit> units);

    @Query("SELECT * FROM " + Utils.UNIT_TABLE_NAME + " WHERE unit_id = :unitId")
    public abstract Unit getUnit(int unitId);

    @Insert(onConflict = REPLACE)
    public abstract void insertStatusesList(List<Statuses> statuses);

    @Query("SELECT * FROM " + Utils.STATUS_TABLE_NAME + " WHERE status_id = :statusId")
    public abstract Statuses getStatus(int statusId);

    @Query("SELECT * FROM " + Utils.UNIT_TABLE_NAME)
    public abstract List<Unit> getAllUnits();

    @Query("SELECT * FROM " + Utils.STATUS_TABLE_NAME)
    public abstract List<Statuses> getAllStatuses();

    @Query("SELECT status_id FROM " + Utils.STATUS_TABLE_NAME + " WHERE name LIKE '%' || :statusName || '%'")
    public abstract Integer getStatusId(String statusName);

    @Transaction
    @Query("SELECT * FROM " + Utils.FORMS_TABLE_NAME + " WHERE form_id = :formId AND client_id = :clientId AND job_id = :jobId AND job_store_id= :jobStoreId AND store_id = :storeId")
    public abstract DisplayForm getFormDisplay(int formId, int clientId, int jobId, int jobStoreId, int storeId);

    @Transaction
    @Query("SELECT * FROM " + Utils.LOCAL_FORMS_TABLE_NAME + " WHERE form_id = :formId AND client_id = :clientId AND job_id = :jobId AND job_store_id= :jobStoreId AND store_id = :storeId")
    public abstract DisplayLocalForm getLocalFormDisplay(int formId, int clientId, int jobId, int jobStoreId, int storeId);

    @Insert(onConflict = REPLACE)
    public abstract Long insertLocalForm(LocalForm localForm);

    @Query("DELETE FROM " + Utils.LOCAL_FORMS_TABLE_NAME + " WHERE form_id = :formId AND job_store_id = :jobStoreId")
    public abstract int deleteLocalForm(int formId, int jobStoreId);

}
