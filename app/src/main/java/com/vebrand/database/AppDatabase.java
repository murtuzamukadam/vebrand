package com.vebrand.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.vebrand.model.Clients;
import com.vebrand.model.Form;
import com.vebrand.model.JobStores;
import com.vebrand.model.Jobs;
import com.vebrand.model.LocalForm;
import com.vebrand.model.Statuses;
import com.vebrand.model.Store;
import com.vebrand.model.Unit;
import com.vebrand.utils.Utils;
import com.vebrand.model.User;

@Database(entities = {User.class,Clients.class, Jobs.class, JobStores.class, Store.class, Form.class,
        Unit.class, Statuses.class, LocalForm.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AppDao appDao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, Utils.DATABASE_NAME).allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}