package com.vebrand.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public class BackgroundExecutor {

    public <T> void executeAsync(Callable<T> callable, CallBack<T> callback, Executor executor) {
        executor.execute(() -> {
            final T result;
            try {
                result = callable.call();
                callback.onComplete(result);
            } catch (Exception e) {
                e.printStackTrace();
                callback.onError(e);
            }
        });
    }

    public interface CallBack<T> {
        void onComplete(T result);
        void onError(Exception e);
    }
}
