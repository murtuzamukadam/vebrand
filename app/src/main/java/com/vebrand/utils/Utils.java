package com.vebrand.utils;

import static android.content.Context.MODE_PRIVATE;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.channels.FileChannel;

public class Utils {

    public static final String DATABASE_NAME = "vebrand_database";

    public static final String TOKEN = "token";
    public static final String USERNAME = "username";

    public static final String RECCE_TABLE_NAME = "recce_image_models";
    public static final String USER_TABLE_NAME = "user";

    public static final String CLIENT_TABLE_NAME = "client";
    public static final String CLIENT_ID_ROOM = "client_id";

    public static final String JOBS_TABLE_NAME = "jobs";
    public static final String JOBS_ID_ROOM = "job_id";

    public static final String STORE_TABLE_NAME = "stores";
    public static final String STORE_ID_ROOM = "store_id";

    public static final String JOB_STORES_TABLE_NAME = "job_stores";
    public static final String JOB_STORE_ID_ROOM = "job_store_id";

    public static final String FORMS_TABLE_NAME = "forms";
    public static final String FORMS_ID_ROOM = "form_id";
    public static final String LOCAL_FORMS_TABLE_NAME = "local_form";

    public static final String PRODUCT_TABLE_NAME = "products_master";
    public static final String PRODUCT_ID_ROOM = "product_id";

    public static final String WIDTH_TABLE_NAME = "width_master";
    public static final String WIDTH_ID_ROOM = "width_id";

    public static final String HEIGHT_TABLE_NAME = "height_master";
    public static final String HEIGHT_ID_ROOM = "height_id";

    public static final String UNIT_TABLE_NAME = "unit_master";
    public static final String UNIT_ID_ROOM = "unit_id";

    public static final String STATUS_TABLE_NAME = "status_master";
    public static final String STATUS_ID_ROOM = "status_id";

    public static final String RECCE_STATUS_TABLE_NAME = "recce_status_master";
    public static final String RECCE_STATUS_ID_ROOM = "recce_status_id";

    public static final String RECCE_IMAGE = "recce_image";
    public static final String RECCE_IMAGE_1 = "recce_1_image";
    public static final String RECCE_IMAGE_2 = "recce_2_image";
    public static final String RECCE_IMAGE_3 = "recce_3_image";
    public static final String RECCE_IMAGE_4 = "recce_4_image";

    public static final String RECCE_IMAGE_EDITED = "recce_image_edited";
    public static final String RECCE_IMAGE_1_EDITED = "recce_1_image_edited";
    public static final String RECCE_IMAGE_2_EDITED = "recce_2_image_edited";
    public static final String RECCE_IMAGE_3_EDITED = "recce_3_image_edited";
    public static final String RECCE_IMAGE_4_EDITED = "recce_4_image_edited";

    public static final String RECCE_CERTIFICATE = "recce_certificate";
    public static final String RECCE_CERTIFICATE_EDITED = "recce_certificate_edited";

    public static final String BASE_URL = "https://vebrand-endpoint.valuationkart.in/api/";


    public static void copyFileUri(Application context, Uri inputUri, String outPath) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(inputUri);
            FileInputStream fileInputStream = (FileInputStream) inputStream;

            File fileOut = new File(outPath);
            if(fileOut.exists()) fileOut.delete();
            FileOutputStream fileOutputStream = new FileOutputStream(fileOut);
            FileChannel inChannel = fileInputStream.getChannel();
            FileChannel outChannel = fileOutputStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);

            fileInputStream.close();
            inputStream.close();
            fileOutputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getImageCount(File file) {
        File[] list = file.listFiles();
        int count = 0;
        if (list != null) {
            if (list.length > 0) {
                for (File f : list) {
                    String name = f.getName();
                    if (name.endsWith("_unedited_temp.jpg"))
                        count++;
                }
            }

        }
        return count;
    }

    public static boolean isConnect(Application context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            return connectivity.getActiveNetworkInfo() != null && connectivity.getActiveNetworkInfo().isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getToken(Application application){
        SharedPreferences sharedPreferences = application.getSharedPreferences(Utils.TOKEN, MODE_PRIVATE);
        return sharedPreferences.getString(TOKEN,"");
    }

    public static String getBearerToken(Application application){
        SharedPreferences sharedPreferences = application.getSharedPreferences(Utils.TOKEN, MODE_PRIVATE);
        return "Bearer " + sharedPreferences.getString(TOKEN,"");
    }

    public static void insertToken(Application application,String token){
        SharedPreferences sharedPreferences = application.getSharedPreferences(Utils.TOKEN, MODE_PRIVATE);
        sharedPreferences.edit().putString(Utils.TOKEN, token).apply();
    }

    public static String getUserName(Application application){
        SharedPreferences sharedPreferences = application.getSharedPreferences(Utils.TOKEN, MODE_PRIVATE);
        return sharedPreferences.getString(USERNAME,"");
    }

    public static void setUserName(Application application,String token){
        SharedPreferences sharedPreferences = application.getSharedPreferences(Utils.TOKEN, MODE_PRIVATE);
        sharedPreferences.edit().putString(USERNAME, token).apply();
    }

    public static String convertErrorMessage(String errorBody, String TAG) throws JSONException {
        JSONObject jObjError = new JSONObject(errorBody);
        Log.d(TAG, "onResponse: json " + jObjError.getString("message"));
        String message = jObjError.getString("message");
        return message.substring(message.indexOf(":") + 1);
    }
}

