package com.vebrand.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobStoreResponse {

    @Expose
    @SerializedName("data")
    List<JobStores> jobStores;

    public List<JobStores> getJobStores() {
        return jobStores;
    }

    public void setJobStores(List<JobStores> jobStores) {
        this.jobStores = jobStores;
    }
}
