package com.vebrand.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vebrand.utils.Utils;

import java.util.Objects;

public class JobStoresAndStores {

    @Embedded
    JobStores jobStores;

    @Relation(
            parentColumn = Utils.CLIENT_ID_ROOM,
            entityColumn = Utils.CLIENT_ID_ROOM
    )
    Clients clients;

    @Relation(
            parentColumn = Utils.STORE_ID_ROOM,
            entityColumn = Utils.STORE_ID_ROOM
    )
    Store store;


    public JobStores getJobStores() {
        return jobStores;
    }

    public void setJobStores(JobStores jobStores) {
        this.jobStores = jobStores;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobStoresAndStores that = (JobStoresAndStores) o;
        return Objects.equals(jobStores, that.jobStores) && Objects.equals(clients, that.clients) && Objects.equals(store, that.store);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobStores, clients, store);
    }
}
