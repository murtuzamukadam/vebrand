package com.vebrand.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;


@Entity(tableName = Utils.STORE_TABLE_NAME)
public class Store {

    @PrimaryKey
    @ColumnInfo(name = Utils.STORE_ID_ROOM)
    @Expose
    @SerializedName("id")
    int storeId;

    @ColumnInfo(name = "store_name")
    @Expose
    @SerializedName("name")
    String name;

    @ColumnInfo
    @Expose
    @SerializedName("mobile")
    String mobile;

    @ColumnInfo
    @Expose
    @SerializedName("address")
    String address;

    @ColumnInfo()
    @Expose
    @SerializedName("city")
    String area;

    @Ignore
    @Expose
    @SerializedName("client_id")
    int clientId;

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
