package com.vebrand.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;

import java.util.Objects;

@Entity(tableName = Utils.FORMS_TABLE_NAME,
        foreignKeys = {@ForeignKey(entity = Clients.class,
                parentColumns = Utils.CLIENT_ID_ROOM,
                childColumns = Utils.CLIENT_ID_ROOM,
                onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Jobs.class,
                        parentColumns = Utils.JOBS_ID_ROOM,
                        childColumns = Utils.JOBS_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = JobStores.class,
                        parentColumns = Utils.JOB_STORE_ID_ROOM,
                        childColumns = Utils.JOB_STORE_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Store.class,
                        parentColumns = Utils.STORE_ID_ROOM,
                        childColumns = Utils.STORE_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Unit.class,
                        parentColumns = Utils.UNIT_ID_ROOM,
                        childColumns = Utils.HEIGHT_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Unit.class,
                        parentColumns = Utils.UNIT_ID_ROOM,
                        childColumns = Utils.WIDTH_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Statuses.class,
                        parentColumns = Utils.STATUS_ID_ROOM,
                        childColumns = Utils.RECCE_STATUS_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE)})
public class Form {

    @PrimaryKey
    @ColumnInfo(name = Utils.FORMS_ID_ROOM)
    @Expose
    @SerializedName("id")
    int formId;

    @ColumnInfo(name = Utils.CLIENT_ID_ROOM)
    int clientId;

    @ColumnInfo(name = Utils.JOBS_ID_ROOM)
    int jobiId;

    @ColumnInfo(name = Utils.STORE_ID_ROOM)
    int storeId;

    @ColumnInfo(name = Utils.JOB_STORE_ID_ROOM)
    @Expose
    @SerializedName("job_store_id")
    int jobStoreId;

    @ColumnInfo(name = Utils.PRODUCT_ID_ROOM)
    @Expose
    @SerializedName("product_id")
    int productId;

    @ColumnInfo(name = "product_name")
    String productName;

    @ColumnInfo(name = Utils.WIDTH_ID_ROOM)
    @Expose
    @SerializedName("width_unit_id")
    int widthId;

    @ColumnInfo(name = Utils.HEIGHT_ID_ROOM)
    @Expose
    @SerializedName("height_unit_id")
    int heightId;

    @ColumnInfo(name = Utils.RECCE_STATUS_ID_ROOM)
    @Expose
    @SerializedName("recce_job_status_id")
    int recceStatusId;

    @ColumnInfo
    @Expose
    @SerializedName("width")
    int width;

    @ColumnInfo
    @Expose
    @SerializedName("height")
    int height;

    @ColumnInfo
    @Expose
    @SerializedName("quantity")
    int quantity;

    @ColumnInfo(name = Utils.RECCE_IMAGE)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE)
    String image0;

    @ColumnInfo(name = Utils.RECCE_IMAGE_1)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_1)
    String image1;

    @ColumnInfo(name = Utils.RECCE_IMAGE_2)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_2)
    String image2;

    @ColumnInfo(name = Utils.RECCE_IMAGE_3)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_3)
    String image3;

    @ColumnInfo(name = Utils.RECCE_IMAGE_4)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_4)
    String image4;

    @ColumnInfo(name = Utils.RECCE_IMAGE_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_EDITED)
    String image0Edited;

    @ColumnInfo(name = Utils.RECCE_IMAGE_1_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_1_EDITED)
    String image1Edited;

    @ColumnInfo(name = Utils.RECCE_IMAGE_2_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_2_EDITED)
    String image2Edited;

    @ColumnInfo(name = Utils.RECCE_IMAGE_3_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_3_EDITED)
    String image3Edited;

    @ColumnInfo(name = Utils.RECCE_IMAGE_4_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_IMAGE_4_EDITED)
    String image4Edited;

    @ColumnInfo(name = Utils.RECCE_CERTIFICATE)
    @Expose
    @SerializedName(Utils.RECCE_CERTIFICATE)
    String recceCertificate;

    @ColumnInfo(name = Utils.RECCE_CERTIFICATE_EDITED)
    @Expose
    @SerializedName(Utils.RECCE_CERTIFICATE_EDITED)
    String recceCertificateEdited;

    @ColumnInfo(name = "recce_remark")
    @Expose
    @SerializedName("recce_remark")
    String recceRemark;

    @Ignore
    @Expose
    @SerializedName("job_store")
    JobStores jobStores;

    @Ignore
    @Expose
    @SerializedName("product")
    Product product;

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getJobiId() {
        return jobiId;
    }

    public void setJobiId(int jobiId) {
        this.jobiId = jobiId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getWidthId() {
        return widthId;
    }

    public void setWidthId(int widthId) {
        this.widthId = widthId;
    }

    public int getHeightId() {
        return heightId;
    }

    public void setHeightId(int heightId) {
        this.heightId = heightId;
    }

    public int getRecceStatusId() {
        return recceStatusId;
    }

    public void setRecceStatusId(int recceStatusId) {
        this.recceStatusId = recceStatusId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImage0() {
        return image0;
    }

    public void setImage0(String image0) {
        this.image0 = image0;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage0Edited() {
        return image0Edited;
    }

    public void setImage0Edited(String image0Edited) {
        this.image0Edited = image0Edited;
    }

    public String getImage1Edited() {
        return image1Edited;
    }

    public void setImage1Edited(String image1Edited) {
        this.image1Edited = image1Edited;
    }

    public String getImage2Edited() {
        return image2Edited;
    }

    public void setImage2Edited(String image2Edited) {
        this.image2Edited = image2Edited;
    }

    public String getImage3Edited() {
        return image3Edited;
    }

    public void setImage3Edited(String image3Edited) {
        this.image3Edited = image3Edited;
    }

    public String getImage4Edited() {
        return image4Edited;
    }

    public void setImage4Edited(String image4Edited) {
        this.image4Edited = image4Edited;
    }

    public String getRecceCertificate() {
        return recceCertificate;
    }

    public void setRecceCertificate(String recceCertificate) {
        this.recceCertificate = recceCertificate;
    }

    public String getRecceCertificateEdited() {
        return recceCertificateEdited;
    }

    public void setRecceCertificateEdited(String recceCertificateEdited) {
        this.recceCertificateEdited = recceCertificateEdited;
    }

    public int getJobStoreId() {
        return jobStoreId;
    }

    public void setJobStoreId(int jobStoreId) {
        this.jobStoreId = jobStoreId;
    }

    public String getRecceRemark() {
        return recceRemark;
    }

    public void setRecceRemark(String recceRemark) {
        this.recceRemark = recceRemark;
    }

    public JobStores getJobStores() {
        return jobStores;
    }

    public void setJobStores(JobStores jobStores) {
        this.jobStores = jobStores;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return formId == form.formId && clientId == form.clientId && jobiId == form.jobiId && storeId == form.storeId && jobStoreId == form.jobStoreId && productId == form.productId && widthId == form.widthId && heightId == form.heightId && recceStatusId == form.recceStatusId && width == form.width && height == form.height && quantity == form.quantity && Objects.equals(productName, form.productName) && Objects.equals(image0, form.image0) && Objects.equals(image1, form.image1) && Objects.equals(image2, form.image2) && Objects.equals(image3, form.image3) && Objects.equals(image4, form.image4) && Objects.equals(image0Edited, form.image0Edited) && Objects.equals(image1Edited, form.image1Edited) && Objects.equals(image2Edited, form.image2Edited) && Objects.equals(image3Edited, form.image3Edited) && Objects.equals(image4Edited, form.image4Edited) && Objects.equals(recceCertificate, form.recceCertificate) && Objects.equals(recceCertificateEdited, form.recceCertificateEdited) && Objects.equals(recceRemark, form.recceRemark) && Objects.equals(jobStores, form.jobStores) && Objects.equals(product, form.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(formId, clientId, jobiId, storeId, jobStoreId, productId, productName, widthId, heightId, recceStatusId, width, height, quantity, image0, image1, image2, image3, image4, image0Edited, image1Edited, image2Edited, image3Edited, image4Edited, recceCertificate, recceCertificateEdited, recceRemark, jobStores, product);
    }

    @NonNull
    @Override
    public String toString() {
        return "Form{" +
                "formId=" + formId +
                ", clientId=" + clientId +
                ", jobiId=" + jobiId +
                ", storeId=" + storeId +
                ", jobStoreId=" + jobStoreId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", widthId=" + widthId +
                ", heightId=" + heightId +
                ", recceStatusId=" + recceStatusId +
                ", width=" + width +
                ", height=" + height +
                ", quantity=" + quantity +
                ", image0='" + image0 + '\'' +
                ", image1='" + image1 + '\'' +
                ", image2='" + image2 + '\'' +
                ", image3='" + image3 + '\'' +
                ", image4='" + image4 + '\'' +
                ", image0Edited='" + image0Edited + '\'' +
                ", image1Edited='" + image1Edited + '\'' +
                ", image2Edited='" + image2Edited + '\'' +
                ", image3Edited='" + image3Edited + '\'' +
                ", image4Edited='" + image4Edited + '\'' +
                ", recceCertificate='" + recceCertificate + '\'' +
                ", recceCertificateEdited='" + recceCertificateEdited + '\'' +
                ", recceRemark='" + recceRemark + '\'' +
                ", jobStores=" + jobStores +
                ", product=" + product +
                '}';
    }
}
