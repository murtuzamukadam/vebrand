package com.vebrand.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;

import java.util.Objects;

@Entity(tableName = Utils.JOB_STORES_TABLE_NAME,
        foreignKeys = {@ForeignKey(entity = Clients.class,
                parentColumns = Utils.CLIENT_ID_ROOM,
                childColumns = Utils.CLIENT_ID_ROOM,
                onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Jobs.class,
                        parentColumns = Utils.JOBS_ID_ROOM,
                        childColumns = Utils.JOBS_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Store.class,
                        parentColumns = Utils.STORE_ID_ROOM,
                        childColumns = Utils.STORE_ID_ROOM,
                        onUpdate = ForeignKey.CASCADE)})
public class JobStores {

    @PrimaryKey
    @ColumnInfo(name = Utils.JOB_STORE_ID_ROOM)
    @Expose
    @SerializedName("id")
    int jobStoresId;

    @ColumnInfo(name = Utils.STORE_ID_ROOM)
    @Expose
    @SerializedName("store_id")
    int storeId;

    @ColumnInfo(name = Utils.CLIENT_ID_ROOM)
    int clientId;

    @ColumnInfo(name = Utils.JOBS_ID_ROOM)
    @Expose
    @SerializedName("job_id")
    int jobId;

    @Ignore
    @Expose
    @SerializedName("store")
    Store store;

    public int getJobStoresId() {
        return jobStoresId;
    }

    public void setJobStoresId(int jobStoresId) {
        this.jobStoresId = jobStoresId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobStores jobStores = (JobStores) o;
        return jobStoresId == jobStores.jobStoresId && storeId == jobStores.storeId && clientId == jobStores.clientId && jobId == jobStores.jobId && Objects.equals(store, jobStores.store);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobStoresId, storeId, clientId, jobId, store);
    }
}
