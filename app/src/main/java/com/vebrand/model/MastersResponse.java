package com.vebrand.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MastersResponse {

    @Expose
    @SerializedName("data")
    MasterResponse masterResponses;

    public MasterResponse getMasterResponses() {
        return masterResponses;
    }

    public void setMasterResponses(MasterResponse masterResponses) {
        this.masterResponses = masterResponses;
    }
}
