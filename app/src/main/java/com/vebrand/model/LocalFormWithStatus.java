package com.vebrand.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vebrand.utils.Utils;

import java.util.Objects;

public class LocalFormWithStatus {

    @Embedded
    LocalForm localForm;

    @Relation(
            parentColumn = Utils.RECCE_STATUS_ID_ROOM,
            entityColumn = Utils.STATUS_ID_ROOM
    )
    Statuses statuses;

    public LocalForm getLocalForm() {
        return localForm;
    }

    public void setLocalForm(LocalForm localForm) {
        this.localForm = localForm;
    }

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalFormWithStatus that = (LocalFormWithStatus) o;
        return Objects.equals(localForm, that.localForm) && Objects.equals(statuses, that.statuses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localForm, statuses);
    }
}
