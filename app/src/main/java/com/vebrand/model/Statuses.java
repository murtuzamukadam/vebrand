package com.vebrand.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;

@Entity(tableName = Utils.STATUS_TABLE_NAME)
public class Statuses {

    @PrimaryKey
    @ColumnInfo(name = Utils.STATUS_ID_ROOM)
    @Expose
    @SerializedName("id")
    int unitId;

    @ColumnInfo
    @Expose
    @SerializedName("name")
    String name;

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
