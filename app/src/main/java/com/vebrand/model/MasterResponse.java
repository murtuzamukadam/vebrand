package com.vebrand.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterResponse {


    @Expose
    @SerializedName("units")
    List<Unit> unit;

    @Expose
    @SerializedName("job_statuses")
    List<Statuses> statuses;

    public List<Unit> getUnit() {
        return unit;
    }

    public void setUnit(List<Unit> unit) {
        this.unit = unit;
    }

    public List<Statuses> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Statuses> statuses) {
        this.statuses = statuses;
    }
}
