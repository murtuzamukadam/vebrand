package com.vebrand.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "user")
public class User {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "user_id")
    @SerializedName("id")
    @Expose
    String userId;

    @ColumnInfo(name = "role_id")
    @SerializedName("role_id")
    @Expose
    String roleID;

    @ColumnInfo(name = "login")
    @SerializedName("login")
    @Expose
    String login;

    @ColumnInfo
    @SerializedName("token")
    @Expose
    String token;

    @ColumnInfo
    @SerializedName("otp")
    @Expose
    String password;

    @SerializedName("team")
    @Expose
    @Ignore
    Team team;

    @ColumnInfo(name = "firstname", defaultValue = "NULL")
    @Nullable
    String firstName;

    @ColumnInfo(name = "lastname", defaultValue = "NULL")
    @Nullable
    String lastName;

    @ColumnInfo(name = "email", defaultValue = "NULL")
    @Nullable
    String email;

    @ColumnInfo(name = "mobile", defaultValue = "NULL")
    @Nullable
    String mobile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@Nullable String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    public String getMobile() {
        return mobile;
    }

    public void setMobile(@Nullable String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

