package com.vebrand.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;

import java.util.Objects;

@Entity(tableName = Utils.CLIENT_TABLE_NAME)
public class Clients {

    @PrimaryKey
    @ColumnInfo(name = Utils.CLIENT_ID_ROOM)
    @Expose
    @SerializedName("id")
    int clientId;

    @ColumnInfo(name = "client_name")
    @Expose
    @SerializedName("name")
    String clientName;

    @Ignore
    @Expose
    @SerializedName("totals")
    Totals totals;

    @ColumnInfo(name = "total")
    String total;

    @ColumnInfo(name = "pending")
    String pending;

    @ColumnInfo(name = "done")
    String done;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Totals getTotals() {
        return totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clients clients = (Clients) o;
        return clientId == clients.clientId && Objects.equals(clientName, clients.clientName) && Objects.equals(totals, clients.totals) && Objects.equals(total, clients.total) && Objects.equals(pending, clients.pending) && Objects.equals(done, clients.done);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, clientName, totals, total, pending, done);
    }
}

