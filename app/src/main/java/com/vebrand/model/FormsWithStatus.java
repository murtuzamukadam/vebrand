package com.vebrand.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vebrand.utils.Utils;

import java.util.Objects;

public class FormsWithStatus {

    @Embedded
    Form form;


    @Relation(
            parentColumn = Utils.RECCE_STATUS_ID_ROOM,
            entityColumn = Utils.STATUS_ID_ROOM
    )
    Statuses statuses;

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FormsWithStatus that = (FormsWithStatus) o;
        return Objects.equals(form, that.form) && Objects.equals(statuses, that.statuses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(form, statuses);
    }
}
