package com.vebrand.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vebrand.utils.Utils;

import java.util.Objects;

@Entity(tableName = Utils.JOBS_TABLE_NAME,
        foreignKeys = {@ForeignKey(entity = Clients.class,
                parentColumns = Utils.CLIENT_ID_ROOM,
                childColumns = Utils.CLIENT_ID_ROOM,
                onUpdate = ForeignKey.CASCADE)})
public class Jobs {

    @PrimaryKey
    @ColumnInfo(name = Utils.JOBS_ID_ROOM)
    @Expose
    @SerializedName("id")
    int jobId;

    @ColumnInfo(name = "job_name")
    @Expose
    @SerializedName("title")
    String jobName;

    @ColumnInfo(name = "description")
    @Expose
    @SerializedName("description")
    String description;

    @ColumnInfo(name = Utils.CLIENT_ID_ROOM)
    @Expose
    @SerializedName("client_id")
    int clientId;

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jobs jobs = (Jobs) o;
        return jobId == jobs.jobId && clientId == jobs.clientId && Objects.equals(jobName, jobs.jobName) && Objects.equals(description, jobs.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, jobName, description, clientId);
    }
}
