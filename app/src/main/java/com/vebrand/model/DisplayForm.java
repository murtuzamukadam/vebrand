package com.vebrand.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.vebrand.utils.Utils;

public class DisplayForm {

    @Embedded
    Form form;

    @Relation(
            parentColumn = Utils.CLIENT_ID_ROOM,
            entityColumn = Utils.CLIENT_ID_ROOM
    )
    Clients clients;

    @Relation(
            parentColumn = Utils.STORE_ID_ROOM,
            entityColumn = Utils.STORE_ID_ROOM
    )
    Store store;

    @Relation(
            parentColumn = Utils.JOBS_ID_ROOM,
            entityColumn = Utils.JOBS_ID_ROOM
    )
    Jobs jobs;

    @Relation(
            parentColumn = Utils.JOB_STORE_ID_ROOM,
            entityColumn = Utils.JOB_STORE_ID_ROOM
    )
    JobStores jobStores;

    @Relation(
            parentColumn = Utils.HEIGHT_ID_ROOM,
            entityColumn = Utils.UNIT_ID_ROOM
    )
    Unit height;

    @Relation(
            parentColumn = Utils.WIDTH_ID_ROOM,
            entityColumn = Utils.UNIT_ID_ROOM
    )
    Unit width;

    @Relation(
            parentColumn = Utils.RECCE_STATUS_ID_ROOM,
            entityColumn = Utils.STATUS_ID_ROOM
    )
    Statuses statuses;

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Clients getClients() {
        return clients;
    }

    public void setClients(Clients clients) {
        this.clients = clients;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Jobs getJobs() {
        return jobs;
    }

    public void setJobs(Jobs jobs) {
        this.jobs = jobs;
    }

    public JobStores getJobStores() {
        return jobStores;
    }

    public void setJobStores(JobStores jobStores) {
        this.jobStores = jobStores;
    }

    public Unit getHeight() {
        return height;
    }

    public void setHeight(Unit height) {
        this.height = height;
    }

    public Unit getWidth() {
        return width;
    }

    public void setWidth(Unit width) {
        this.width = width;
    }

    public Statuses getStatuses() {
        return statuses;
    }

    public void setStatuses(Statuses statuses) {
        this.statuses = statuses;
    }

    @Override
    public String toString() {
        return "DisplayForm{" +
                "form=" + form +
                ", clients=" + clients +
                ", store=" + store +
                ", jobs=" + jobs +
                ", jobStores=" + jobStores +
                ", height=" + height +
                ", width=" + width +
                ", statuses=" + statuses +
                '}';
    }
}
