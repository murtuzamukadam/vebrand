package com.vebrand.viewmodel;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;


import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.ResponseData;
import com.vebrand.model.User;
import com.vebrand.utils.BackgroundExecutor;

public class LoginViewModel extends AndroidViewModel {

    MutableLiveData<User> requestOTPResponse = new MutableLiveData<>();
    MutableLiveData<User> checkLoginResponse = new MutableLiveData<>();
    MutableLiveData<Pair<RESULT,String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<Boolean> isUserInserted = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    Repository repository;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public void requestOTP(String id){
        isLoading.setValue(true);
        repository.requestOTP(id, requestOTPResponse,checkErrors);
    }

    public void checkLogin(String login, String password){
        isLoading.setValue(true);
        repository.checkLogin(login,password,"login",checkLoginResponse,checkErrors);
    }

    public LiveData<User> getRequestOTPResponse() {
        return requestOTPResponse;
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public LiveData<User> getCheckLoginResponse() {
        return checkLoginResponse;
    }

    public LiveData<Boolean> getIsUserInserted() {
        return isUserInserted;
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }

    public void insertNewUser(User user){
        repository.insertNewUser(user, new BackgroundExecutor.CallBack<Long>() {
            @Override
            public void onComplete(Long result) {
                if(result != -1)
                new Handler(Looper.getMainLooper()).post(() -> {
                    isUserInserted.setValue(Boolean.TRUE);
                    isLoading.setValue(false);
                });
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    checkErrors.setValue(new Pair<>(RESULT.ERROR,e.toString()));
                    isLoading.setValue(false);
                });
            }
        });
    }

}
