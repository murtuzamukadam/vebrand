package com.vebrand.viewmodel;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.JobStores;
import com.vebrand.model.JobStoresAndStores;
import com.vebrand.utils.BackgroundExecutor;

import java.util.List;

public class JobStoresViewModel extends AndroidViewModel {

    private static final String TAG = JobStores.class.getSimpleName();
    MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    MutableLiveData<List<JobStores>> jobStoresApi = new MutableLiveData<>();
    MutableLiveData<List<JobStores>> jobStoresListDatabase = new MutableLiveData<>();
    MutableLiveData<List<JobStoresAndStores>> jobStoresAndStoresListDatabase = new MutableLiveData<>();
    int clientId, jobId;
    Repository repository;

    public JobStoresViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public void setCheckErrors(Pair<RESULT, String> checkErrors) {
        this.checkErrors.setValue(checkErrors);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }

    public LiveData<List<JobStores>> getJobStoresApi() {
        return jobStoresApi;
    }

    public void setJobStoresApi(List<JobStores> jobStoresApi) {
        this.jobStoresApi.setValue(jobStoresApi);
    }

    public LiveData<List<JobStores>> getJobStoresListDatabase() {
        return jobStoresListDatabase;
    }

    public void setJobStoresListDatabase(List<JobStores> jobStoresListDatabase) {
        this.jobStoresListDatabase.setValue(jobStoresListDatabase);
    }

    public LiveData<List<JobStoresAndStores>> getJobStoresAndStoresListDatabase() {
        return jobStoresAndStoresListDatabase;
    }

    public void setJobStoresAndStoresListDatabase(List<JobStoresAndStores> jobStoresAndStoresListDatabase) {
        this.jobStoresAndStoresListDatabase.setValue(jobStoresAndStoresListDatabase);
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public void getJobStoreFromApi() {
        setIsLoading(true);
        repository.getJobStores(jobStoresApi, checkErrors, String.valueOf(jobId));
    }

    public void insertJobStoreListIntoDatabase(List<JobStores> jobsStores) {

        for(JobStores j :jobsStores){
            j.setClientId(j.getStore().getClientId());
            repository.insertStore(j.getStore());
        }

        repository.insertJobStoreList(jobsStores, new BackgroundExecutor.CallBack<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                Log.d(TAG, "onComplete: jobsStore.size " + jobsStores.size() + " " + result.size());
                if (result.size() == jobsStores.size()) {
                    getJobStoreListFromDatabase();
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Inserting Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

    public void getJobStoreListFromDatabase() {
        //new Handler(Looper.getMainLooper()).post(() -> setIsLoading(true));
        repository.getJobStoresAndStoresListFromDatabase(clientId, jobId, new BackgroundExecutor.CallBack<List<JobStoresAndStores>>() {
            @Override
            public void onComplete(List<JobStoresAndStores> result) {
                if (result != null) {
                    new Handler(Looper.getMainLooper()).post(() -> jobStoresAndStoresListDatabase.setValue(result));
                    //Log.d(TAG, "onComplete: " + result.get(0).getJobStores().getJobStoresId() + " store " + result.get(0).getStore().getStoreId() + " client " + result.get(0).getClients().getClientName() );
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }
}
