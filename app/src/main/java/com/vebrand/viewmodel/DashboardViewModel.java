package com.vebrand.viewmodel;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.User;
import com.vebrand.utils.BackgroundExecutor;

public class DashboardViewModel extends AndroidViewModel {

    Repository repository;

    public MutableLiveData<Pair<Boolean, User>> userMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();

    public DashboardViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public void getUser() {
        repository.getUser(new BackgroundExecutor.CallBack<User>() {
            @Override
            public void onComplete(User result) {
                if (result != null && result.getId() != 0)
                    new Handler(Looper.getMainLooper()).post(() -> userMutableLiveData.setValue(new Pair<>(true, result)));
                else
                    new Handler(Looper.getMainLooper()).post(() -> userMutableLiveData.setValue(new Pair<>(false, null)));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

    public LiveData<Pair<Boolean, User>> getUserMutableLiveData() {
        return userMutableLiveData;
    }

    public MutableLiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public void getMasters(){
        repository.getMasters();
    }
}
