package com.vebrand.viewmodel;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.Jobs;
import com.vebrand.utils.BackgroundExecutor;

import java.util.List;

public class JobViewModel extends AndroidViewModel {

    private static final String TAG = JobViewModel.class.getSimpleName();
    MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    MutableLiveData<List<Jobs>> jobsApi = new MutableLiveData<>();
    MutableLiveData<List<Jobs>> jobsListDatabase = new MutableLiveData<>();
    int clientId;
    Repository repository;


    public JobViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public void setCheckErrors(Pair<RESULT, String> checkErrors) {
        this.checkErrors.setValue(checkErrors);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }

    public LiveData<List<Jobs>> getJobsApi() {
        return jobsApi;
    }

    public void setJobsApi(List<Jobs> jobsApi) {
        this.jobsApi.setValue(jobsApi);
    }

    public LiveData<List<Jobs>> getJobsListDatabase() {
        return jobsListDatabase;
    }

    public void setJobsListDatabase(List<Jobs> jobsListDatabase) {
        this.jobsListDatabase.setValue(jobsListDatabase);
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void getJobsFromApi() {
        setIsLoading(true);
        repository.getJobs(jobsApi, checkErrors, String.valueOf(clientId));
    }

    public void insertJobListIntoDatabase(List<Jobs> jobs) {
        repository.insertJobList(jobs, new BackgroundExecutor.CallBack<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                Log.d(TAG, "onComplete: jobs.size " + jobs.size() + " " + result.size());
                if (result.size() == jobs.size()) {

                    getJobsListFromDatabase();
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Inserting Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

    public void getJobsListFromDatabase() {
        //new Handler(Looper.getMainLooper()).post(() -> setIsLoading(true));
        repository.getJobListFromDatabase(clientId, new BackgroundExecutor.CallBack<List<Jobs>>() {
            @Override
            public void onComplete(List<Jobs> result) {
                if (result != null) {
                    new Handler(Looper.getMainLooper()).post(() -> jobsListDatabase.setValue(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }
}