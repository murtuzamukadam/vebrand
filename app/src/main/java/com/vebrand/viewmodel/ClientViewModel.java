package com.vebrand.viewmodel;


import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.Clients;
import com.vebrand.model.Totals;
import com.vebrand.utils.BackgroundExecutor;

import java.util.List;

//Common ViewModel Class for clients and jobs
public class ClientViewModel extends AndroidViewModel {

    MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    MutableLiveData<List<Clients>> clientsListApi = new MutableLiveData<>();
    MutableLiveData<List<Clients>> clientsListDatabase = new MutableLiveData<>();
    Repository repository;

    public ClientViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public void setCheckErrors(Pair<RESULT, String> checkErrors) {
        this.checkErrors.setValue(checkErrors);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }

    public LiveData<List<Clients>> getClientsListApi() {
        return clientsListApi;
    }

    public void setClientsListApi(List<Clients> clientsListApi) {
        this.clientsListApi.setValue(clientsListApi);
    }

    public LiveData<List<Clients>> getClientsListDatabase() {
        return clientsListDatabase;
    }

    public void setClientsListDatabase(List<Clients> clientsListDatabase) {
        this.clientsListDatabase.setValue(clientsListDatabase);
    }

    public void getClientsFromApi() {
        setIsLoading(true);
        repository.getClients(clientsListApi, checkErrors);
    }

    public void insertClientListIntoDatabase(List<Clients> clients) {

        for(Clients c: clients){
            c.setTotal(c.getTotals().getTotal());
            c.setPending(c.getTotals().getPending());
            c.setDone(c.getTotals().getDone());
        }

        repository.insertClientList(clients, new BackgroundExecutor.CallBack<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                if (result.size() != 0) {
                    getClientsListFromDatabase();
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Inserting Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

    public void getClientsListFromDatabase() {
        new Handler(Looper.getMainLooper()).post(() -> setIsLoading(true));
        repository.getClientListFromDatabase(new BackgroundExecutor.CallBack<List<Clients>>() {
            @Override
            public void onComplete(List<Clients> result) {
                if(result != null){
                    new Handler(Looper.getMainLooper()).post(() ->clientsListDatabase.setValue(result));
                }
                else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }


}
