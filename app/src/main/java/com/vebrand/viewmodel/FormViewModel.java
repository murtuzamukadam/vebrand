package com.vebrand.viewmodel;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.Form;
import com.vebrand.model.FormsWithStatus;
import com.vebrand.model.LocalFormWithStatus;
import com.vebrand.utils.BackgroundExecutor;

import java.util.List;

public class FormViewModel extends AndroidViewModel {

    private static final String TAG = FormViewModel.class.getSimpleName();
    MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    MutableLiveData<String> storeName = new MutableLiveData<>();
    MutableLiveData<List<Form>> formListApi = new MutableLiveData<>();
    MutableLiveData<List<FormsWithStatus>> formListDatabase = new MutableLiveData<>();
    MutableLiveData<List<LocalFormWithStatus>> localFormListDatabase = new MutableLiveData<>();
    MutableLiveData<Boolean> isUpdateSuccessful = new MutableLiveData<>();
    int clientId, jobId, storeId, jobStoreId;
    Repository repository;

    public FormViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public void setCheckErrors(Pair<RESULT, String> checkErrors) {
        this.checkErrors.setValue(checkErrors);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getJobStoreId() {
        return jobStoreId;
    }

    public void setJobStoreId(int jobStoreId) {
        this.jobStoreId = jobStoreId;
    }

    public void setStoreName(MutableLiveData<String> storeName) {
        this.storeName = storeName;
    }

    public MutableLiveData<String> getStoreName() {
        return storeName;
    }

    public LiveData<List<Form>> getFormListApi() {
        return formListApi;
    }

    public void setFormListApi(List<Form> formListApi) {
        this.formListApi.setValue(formListApi);
    }

    public LiveData<List<FormsWithStatus>> getFormListDatabase() {
        return formListDatabase;
    }

    public void setFormListDatabase(List<FormsWithStatus> formListDatabase) {
        this.formListDatabase.setValue(formListDatabase);
    }

    public void setFormListDatabase(MutableLiveData<List<FormsWithStatus>> formListDatabase) {
        this.formListDatabase = formListDatabase;
    }

    public LiveData<List<LocalFormWithStatus>> getLocalFormListDatabase() {
        return localFormListDatabase;
    }

    public void setLocalFormListDatabase(List<LocalFormWithStatus> localFormListDatabase) {
        this.localFormListDatabase.setValue(localFormListDatabase);
    }

    public LiveData<Boolean> getIsUpdateSuccessful() {
        return isUpdateSuccessful;
    }

    public void setIsUpdateSuccessful(Boolean isUpdateSuccessful) {
        this.isUpdateSuccessful.setValue(isUpdateSuccessful);
    }

    public void getStoreNameFromDatabase() {
        repository.getStoreNameFromDatabase(storeId, new BackgroundExecutor.CallBack<String>() {
            @Override
            public void onComplete(String result) {
                if (result != null)
                    new Handler(Looper.getMainLooper()).post(() -> storeName.setValue(result));
            }

            @Override
            public void onError(Exception e) {

            }
        });
    }

    public void getFormsFromApi() {
        setIsLoading(true);
        repository.getForms(formListApi, checkErrors, String.valueOf(jobStoreId));
    }

    public void insertFormListIntoDatabase(List<Form> forms) {

        for (Form f : forms) {
            f.setClientId(f.getJobStores().getStore().getClientId());
            f.setJobiId(f.getJobStores().getJobId());
            f.setStoreId(f.getJobStores().getStoreId());
            f.setProductId(f.getProduct().getId());
            f.setProductName(f.getProduct().getName());
        }

        repository.insertFormList(forms, new BackgroundExecutor.CallBack<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                Log.d(TAG, "onComplete: forms.size " + forms.size() + " " + result.size());
                if (result.size() == forms.size()) {

                    getFormsListStatusesFromDatabase();
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Inserting Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

//    public void getFormsListFromDatabase() {
//        Log.d(TAG, "getFormsListFromDatabase: " + clientId + " " + jobId + " " + jobStoreId+ " " + storeId);
//        repository.getFormListFromDatabase(clientId, jobId, jobStoreId, storeId, new BackgroundExecutor.CallBack<List<Form>>() {
//            @Override
//            public void onComplete(List<Form> result) {
//                if (result != null) {
//                    Log.d(TAG, "onComplete: formsize " + result.size());
//                    new Handler(Looper.getMainLooper()).post(() -> formListDatabase.setValue(result));
//                } else
//                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
//            }
//
//            @Override
//            public void onError(Exception e) {
//                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
//            }
//        });
//
//    }

    public void getFormsListStatusesFromDatabase() {
        Log.d(TAG, "getFormsListFromDatabase: " + clientId + " " + jobId + " " + jobStoreId+ " " + storeId);
        repository.getFormListStatusesFromDatabase(clientId, jobId, jobStoreId, storeId, new BackgroundExecutor.CallBack<List<FormsWithStatus>>() {
            @Override
            public void onComplete(List<FormsWithStatus> result) {
                if (result != null) {
                    Log.d(TAG, "onComplete: formsize " + result.size());
                    new Handler(Looper.getMainLooper()).post(() -> formListDatabase.setValue(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });

    }

    public void getLocalFormsListStatusesFromDatabase() {
        Log.d(TAG, "getFormsListFromDatabase: " + clientId + " " + jobId + " " + jobStoreId+ " " + storeId);
        repository.getLocalFormListStatusesFromDatabase(clientId, jobId, jobStoreId, storeId, new BackgroundExecutor.CallBack<List<LocalFormWithStatus>>() {
            @Override
            public void onComplete(List<LocalFormWithStatus> result) {
                if (result != null) {
                    Log.d(TAG, "onComplete: formsize " + result.size());
                    new Handler(Looper.getMainLooper()).post(() -> localFormListDatabase.setValue(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }

    public void updateFormStatus(String formId){
        int doneStatusId = repository.getDatabase().appDao().getStatusId("done");
        Log.d(TAG, "updateFormStatus: doneStatusId " + doneStatusId);
        repository.updateFormStatus(formId,String.valueOf(doneStatusId),"recce",isUpdateSuccessful,checkErrors);
    }
}
