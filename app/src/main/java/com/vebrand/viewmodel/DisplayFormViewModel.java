package com.vebrand.viewmodel;

import android.app.Application;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.vebrand.Repository;
import com.vebrand.model.DisplayForm;
import com.vebrand.model.Product;
import com.vebrand.model.Statuses;
import com.vebrand.model.Unit;
import com.vebrand.utils.BackgroundExecutor;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DisplayFormViewModel extends AndroidViewModel {

    private static final String TAG = DisplayFormViewModel.class.getSimpleName();
    MutableLiveData<Pair<RESULT, String>> checkErrors = new MutableLiveData<>();
    MutableLiveData<DisplayForm> displayForm = new MutableLiveData<>();
    MutableLiveData<List<Unit>> unitList = new MutableLiveData<>();
    MutableLiveData<List<Product>> productList = new MutableLiveData<>();
    MutableLiveData<List<Statuses>> statusesList = new MutableLiveData<>();
    MutableLiveData<Integer> imageNumber = new MutableLiveData<>();
    MutableLiveData<Boolean> isLocalFormInserted = new MutableLiveData<>();
    MutableLiveData<Boolean> isUpdateSuccessful = new MutableLiveData<>();
    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    int clientId, jobId, storeId, jobStoreId, formId;
    Repository repository;


    public DisplayFormViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.repositoryInstance(application);
    }

    public LiveData<Pair<RESULT, String>> getCheckErrors() {
        return checkErrors;
    }

    public LiveData<DisplayForm> getDisplayForm() {
        return displayForm;
    }

    public void setDisplayForm(DisplayForm displayForm) {
        this.displayForm.setValue(displayForm);
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getJobStoreId() {
        return jobStoreId;
    }

    public void setJobStoreId(int jobStoreId) {
        this.jobStoreId = jobStoreId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public LiveData<List<Unit>> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList.setValue(unitList);
    }

    public LiveData<List<Product>> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList.setValue(productList);
    }

    public LiveData<List<Statuses>> getStatusesList() {
        return statusesList;
    }

    public void setStatusesList(List<Statuses> statusesList) {
        this.statusesList.setValue(statusesList);
    }

    public LiveData<Integer> getImageNumber() {
        return imageNumber;
    }

    public void setImageNumber(Integer imageNumber) {
        this.imageNumber.setValue(imageNumber);
    }

    public LiveData<Boolean> getIsLocalFormInserted() {
        return isLocalFormInserted;
    }

    public LiveData<Boolean> getIsUpdateSuccessful() {
        return isUpdateSuccessful;
    }

    public void setIsUpdateSuccessful(Boolean isUpdateSuccessful) {
        this.isUpdateSuccessful.setValue(isUpdateSuccessful);
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public void setIsLoading(Boolean isLoading) {
        this.isLoading.setValue(isLoading);
    }


    public void getDisplayFormFromDatabase() {
        //new Handler(Looper.getMainLooper()).post(() -> setIsLoading(true));
        repository.getDisplayFormFromDatabase(formId, clientId, jobId, jobStoreId, storeId, new BackgroundExecutor.CallBack<DisplayForm>() {
            @Override
            public void onComplete(DisplayForm result) {
                if (result != null) {
                    new Handler(Looper.getMainLooper()).post(() -> setDisplayForm(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }

    public void getUnitsFromDatabase() {
        repository.getAllUnitsFromDatabase(new BackgroundExecutor.CallBack<List<Unit>>() {
            @Override
            public void onComplete(List<Unit> result) {
                if (result != null) {
                    new Handler(Looper.getMainLooper()).post(() -> setUnitList(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, "Database Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }

    public void getProductsFromApi() {
        repository.getProducts(String.valueOf(jobStoreId), productList);
    }

    public void copyFileUri(Uri inputUri, String nonEditedPath, int imageNo) {


        repository.copyFileUri(inputUri, nonEditedPath, imageNo, new BackgroundExecutor.CallBack<Integer>() {
            @Override
            public void onComplete(Integer result) {
                if (result != null) {
                    new Handler(Looper.getMainLooper()).post(() -> imageNumber.setValue(result));
                } else
                    new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new androidx.core.util.Pair<>(RESULT.ERROR, "Copying Error")));
            }

            @Override
            public void onError(Exception e) {
                new Handler(Looper.getMainLooper()).post(() -> checkErrors.setValue(new androidx.core.util.Pair<>(RESULT.ERROR, e.toString())));
            }
        });
    }

    public void updateFormApi(DisplayForm displayForm, boolean isImage0Exists, boolean isImage1Exists, boolean isImage2Exists,
                              boolean isImage3Exists, boolean isImage4Exists, boolean isCertificateExists) {
        String id = String.valueOf(displayForm.getForm().getFormId());
        String jobStoreId = String.valueOf(displayForm.getForm().getJobStoreId());
        String productId = String.valueOf(displayForm.getForm().getProductId());
        String width = String.valueOf(displayForm.getForm().getWidth());
        String height = String.valueOf(displayForm.getForm().getHeight());
        String heightUnitId = String.valueOf(displayForm.getForm().getHeightId());
        String widthUnitId = String.valueOf(displayForm.getForm().getWidthId());
        String quantity = String.valueOf(displayForm.getForm().getQuantity());
        String recceRemark = String.valueOf(displayForm.getForm().getRecceRemark());
        String type = "recce";

        RequestBody idRequest = RequestBody.create(id, MediaType.parse("text/plain"));

        RequestBody jobStoreIdRequest = RequestBody.create(jobStoreId, MediaType.parse("text/plain"));

        RequestBody productIdRequest = RequestBody.create(productId, MediaType.parse("text/plain"));

        RequestBody widthRequest = RequestBody.create(width, MediaType.parse("text/plain"));

        RequestBody widthIdRequest = RequestBody.create(widthUnitId, MediaType.parse("text/plain"));

        RequestBody heightRequest = RequestBody.create(height, MediaType.parse("text/plain"));

        RequestBody heightIdRequest = RequestBody.create(heightUnitId, MediaType.parse("text/plain"));

        RequestBody quantityRequest = RequestBody.create(quantity, MediaType.parse("text/plain"));

        RequestBody recceRemarkRequest = RequestBody.create(recceRemark, MediaType.parse("text/plain"));

        RequestBody typeRequest = RequestBody.create(type, MediaType.parse("text/plain"));

        RequestBody recceImageRequest;
        MultipartBody.Part recceImageBody;
        RequestBody recceImageEditedRequest;
        MultipartBody.Part recceImageEditedBody;
        if (isImage0Exists) {
            recceImageRequest = RequestBody.create(new File(displayForm.getForm().getImage0()), MediaType.parse("image/*"));
            recceImageBody = MultipartBody.Part.createFormData("recce_image", "recce_image.jpg", recceImageRequest);
            recceImageEditedRequest = RequestBody.create(new File(displayForm.getForm().getImage0Edited()), MediaType.parse("image/*"));
            recceImageEditedBody = MultipartBody.Part.createFormData("recce_image_edited", "recce_image_edited.jpg", recceImageEditedRequest);
        } else {
            recceImageRequest = RequestBody.create("", MediaType.parse("text/plain"));
            recceImageBody = MultipartBody.Part.createFormData("recce_image", "", recceImageRequest);
            recceImageEditedRequest = RequestBody.create("", MediaType.parse("text/plain"));
            recceImageEditedBody = MultipartBody.Part.createFormData("recce_image_edited", "", recceImageEditedRequest);
        }

        RequestBody recce1ImageRequest;
        MultipartBody.Part recce1ImageBody;
        RequestBody recce1ImageEditedRequest;
        MultipartBody.Part recce1ImageEditedBody;
        if (isImage1Exists) {
            recce1ImageRequest = RequestBody.create(new File(displayForm.getForm().getImage1()), MediaType.parse("image/*"));
            recce1ImageBody = MultipartBody.Part.createFormData("recce_1_image", "recce_1_image.jpg", recce1ImageRequest);
            recce1ImageEditedRequest = RequestBody.create(new File(displayForm.getForm().getImage1Edited()), MediaType.parse("image/*"));
            recce1ImageEditedBody = MultipartBody.Part.createFormData("recce_1_image_edited", "recce_1_image_edited.jpg", recce1ImageEditedRequest);
        } else {
            recce1ImageBody = null;
            recce1ImageEditedBody = null;
        }

        RequestBody recce2ImageRequest;
        MultipartBody.Part recce2ImageBody;
        RequestBody recce2ImageEditedRequest;
        MultipartBody.Part recce2ImageEditedBody;
        if (isImage2Exists) {
            recce2ImageRequest = RequestBody.create(new File(displayForm.getForm().getImage2()), MediaType.parse("image/*"));
            recce2ImageBody = MultipartBody.Part.createFormData("recce_2_image", "recce_2_image.jpg", recce2ImageRequest);
            recce2ImageEditedRequest = RequestBody.create(new File(displayForm.getForm().getImage2Edited()), MediaType.parse("image/*"));
            recce2ImageEditedBody = MultipartBody.Part.createFormData("recce_2_image_edited", "recce_2_image_edited.jpg", recce2ImageEditedRequest);
        } else {
            recce2ImageBody = null;
            recce2ImageEditedBody = null;
        }

        RequestBody recce3ImageRequest;
        MultipartBody.Part recce3ImageBody;
        RequestBody recce3ImageEditedRequest;
        MultipartBody.Part recce3ImageEditedBody;
        if (isImage3Exists) {
            recce3ImageRequest = RequestBody.create(new File(displayForm.getForm().getImage3()), MediaType.parse("image/*"));
            recce3ImageBody = MultipartBody.Part.createFormData("recce_3_image", "recce_3_image.jpg", recce3ImageRequest);
            recce3ImageEditedRequest = RequestBody.create(new File(displayForm.getForm().getImage3Edited()), MediaType.parse("image/*"));
            recce3ImageEditedBody = MultipartBody.Part.createFormData("recce_3_image_edited", "recce_3_image_edited.jpg", recce3ImageEditedRequest);
        } else {
            recce3ImageBody = null;
            recce3ImageEditedBody = null;
        }

        RequestBody recce4ImageRequest;
        MultipartBody.Part recce4ImageBody;
        RequestBody recce4ImageEditedRequest;
        MultipartBody.Part recce4ImageEditedBody;
        if (isImage4Exists) {
            recce4ImageRequest = RequestBody.create(new File(displayForm.getForm().getImage4()), MediaType.parse("image/*"));
            recce4ImageBody = MultipartBody.Part.createFormData("recce_4_image", "recce_4_image.jpg", recce4ImageRequest);
            recce4ImageEditedRequest = RequestBody.create(new File(displayForm.getForm().getImage4Edited()), MediaType.parse("image/*"));
            recce4ImageEditedBody = MultipartBody.Part.createFormData("recce_4_image_edited", "recce_4_image_edited.jpg", recce4ImageEditedRequest);
        } else {
            recce4ImageBody = null;
            recce4ImageEditedBody = null;
        }

        RequestBody recceCertificateRequest;
        MultipartBody.Part recceCertificateBody;
        if (isCertificateExists) {
            recceCertificateRequest = RequestBody.create(new File(displayForm.getForm().getRecceCertificate()), MediaType.parse("image/*"));
            recceCertificateBody = MultipartBody.Part.createFormData("recce_certificate", "recce_certificate.jpg", recceCertificateRequest);
        } else {
            recceCertificateBody = null;
        }

        repository.updateFormApi(idRequest, jobStoreIdRequest, productIdRequest, widthRequest, widthIdRequest, heightRequest,
                heightIdRequest, quantityRequest, recceRemarkRequest, typeRequest, recceImageBody, recceImageEditedBody, recce1ImageBody,
                recce1ImageEditedBody,recce2ImageBody,recce2ImageEditedBody,recce3ImageBody, recce3ImageEditedBody,
                recce4ImageBody,recce4ImageEditedBody,recceCertificateBody,checkErrors, isUpdateSuccessful);

    }
}
