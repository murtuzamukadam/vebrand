package com.vebrand.activities.recce;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.activities.DashboardActivity;
import com.vebrand.adapters.JobsRecyclerViewAdapter;
import com.vebrand.databinding.ActivityRecceJobsBinding;
import com.vebrand.model.Jobs;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.JobViewModel;
import com.vebrand.viewmodel.RESULT;

import java.util.List;

public class JobsActivity extends AppCompatActivity implements ItemClickListener {

    private static final String TAG = JobsActivity.class.getSimpleName();
    ActivityRecceJobsBinding activityRecceJobsBinding;

    JobViewModel jobViewModel;

    List<Jobs> jobsList;
    RecyclerView recyclerView;
    JobsRecyclerViewAdapter jobsRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRecceJobsBinding = ActivityRecceJobsBinding.inflate(getLayoutInflater());
        setContentView(activityRecceJobsBinding.getRoot());
        init();

        int clientId = getIntent().getIntExtra(Utils.CLIENT_ID_ROOM, -1);
        if (clientId != -1) {
            jobViewModel.setIsLoading(true);
            jobViewModel.setClientId(clientId);
            jobViewModel.getJobsListFromDatabase();
            if (Utils.isConnect(getApplication()))
                jobViewModel.getJobsFromApi();
            else showToast("No Internet Detected!", Toast.LENGTH_LONG);
        } else {
            showToast("Something Went Wrong!");
            finish();
        }

        setOnClickListeners();
        setViewModelObservables();

    }

    private void setViewModelObservables() {
        jobViewModel.getJobsListDatabase().observe(this, jobs -> {
            if (jobs.size() != 0) {
                jobViewModel.setIsLoading(false);
                if (jobsList == null) {
                    jobsList = jobs;
                } else {
                    jobsList.clear();
                    jobsList.addAll(jobs);
                }

                if (jobsRecyclerViewAdapter == null) {
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        jobsRecyclerViewAdapter = new JobsRecyclerViewAdapter(this, jobsList);
                        jobsRecyclerViewAdapter.setHasStableIds(true);
                        recyclerView.setAdapter(jobsRecyclerViewAdapter);
                        jobsRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    jobsRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });

        jobViewModel.getJobsApi().observe(this, jobs -> {
            if (jobs.size() != 0)
                jobViewModel.insertJobListIntoDatabase(jobs);
            else {
                jobViewModel.setIsLoading(false);
                showToast("No Jobs!");
            }
        });

        jobViewModel.getCheckErrors().observe(this, resultStringPair -> {
            jobViewModel.setIsLoading(false);
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                showToast(resultStringPair.second);
            }
        });

        jobViewModel.getIsLoading().observe(this, isLoading -> {
            Log.d(TAG, "setViewModelObservables: isLoading " + isLoading);
            if (isLoading)
                activityRecceJobsBinding.progressbarJobs.getRoot().setVisibility(View.VISIBLE);
            else activityRecceJobsBinding.progressbarJobs.getRoot().setVisibility(View.GONE);

        });
    }

    private void setOnClickListeners() {
        activityRecceJobsBinding.imageBackRecceJobs.setOnClickListener(v -> onBackPressed());

        activityRecceJobsBinding.imageHomeRecceJobs.setOnClickListener(v -> {
            startActivity(new Intent(JobsActivity.this, DashboardActivity.class));
            finish();
        });
    }

    @Override
    public void onItemClick(int position) {
        if (jobsList != null) {
            int clientId = jobsList.get(position).getClientId();
            int jobId = jobsList.get(position).getJobId();
            Intent intent = new Intent(JobsActivity.this, JobStoresActivity.class);
            intent.putExtra(Utils.CLIENT_ID_ROOM, clientId);
            intent.putExtra(Utils.JOBS_ID_ROOM, jobId);
            startActivity(intent);
        }
    }

    private void init() {
        recyclerView = activityRecceJobsBinding.recyclerviewRecceJobs;

        jobViewModel = new ViewModelProvider(this).get(JobViewModel.class);

        activityRecceJobsBinding.appbarRecceJobs.appbarUserText.setText(Utils.getUserName(getApplication()));
    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }
}