package com.vebrand.activities.recce;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.activities.DashboardActivity;
import com.vebrand.adapters.JobStoresRecyclerViewAdapter;
import com.vebrand.databinding.ActivityRecceJobStoresBinding;
import com.vebrand.model.JobStoresAndStores;
import com.vebrand.model.Unit;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.JobStoresViewModel;
import com.vebrand.viewmodel.RESULT;

import java.util.ArrayList;
import java.util.List;

public class JobStoresActivity extends AppCompatActivity implements ItemClickListener {

    private static final String TAG = JobStoresActivity.class.getSimpleName();
    private static final String storeName = "Store Name";
    private static final String address = "Address";
    private static final String area = "Area";
    ActivityRecceJobStoresBinding activityRecceJobStoresBinding;

    JobStoresViewModel jobStoresViewModel;

    String filterType;
    Spinner filterTypeSpinner;

    RecyclerView recyclerView;
    JobStoresRecyclerViewAdapter jobStoresRecyclerViewAdapter;
    List<JobStoresAndStores> jobStoresAndStoresList;
    ConstraintLayout recceConstraintLayout, doneConstraintLayout, revisitConstraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRecceJobStoresBinding = ActivityRecceJobStoresBinding.inflate(getLayoutInflater());
        setContentView(activityRecceJobStoresBinding.getRoot());
        init();

        int clientId = getIntent().getIntExtra(Utils.CLIENT_ID_ROOM, -1);
        int jobId = getIntent().getIntExtra(Utils.JOBS_ID_ROOM, -1);
        if (clientId != -1 && jobId != -1) {
            jobStoresViewModel.setIsLoading(true);
            jobStoresViewModel.setClientId(clientId);
            jobStoresViewModel.setJobId(jobId);
            jobStoresViewModel.getJobStoreListFromDatabase();
            if (Utils.isConnect(getApplication()))
                jobStoresViewModel.getJobStoreFromApi();
            else showToast("No Internet Detected!", Toast.LENGTH_LONG);
        } else {
            showToast("Something Went Wrong!");
            finish();
        }
        setOnClickListeners();
        setViewModelObservables();

    }

    private void setViewModelObservables() {

        jobStoresViewModel.getJobStoresApi().observe(this, jobStores -> {
            if (jobStores.size() != 0) {
                jobStoresViewModel.insertJobStoreListIntoDatabase(jobStores);
            } else {
                jobStoresViewModel.setIsLoading(false);
                showToast("No Stores!");
            }
        });

        jobStoresViewModel.getJobStoresAndStoresListDatabase().observe(this, jobStoresAndStores -> {
            if (jobStoresAndStores.size() != 0) {
                jobStoresViewModel.setIsLoading(false);
                if (jobStoresAndStoresList == null) {
                    jobStoresAndStoresList = jobStoresAndStores;
                } else {
                    jobStoresAndStoresList.clear();
                    jobStoresAndStoresList.addAll(jobStoresAndStores);
                }

                if (jobStoresRecyclerViewAdapter == null) {
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        jobStoresRecyclerViewAdapter = new JobStoresRecyclerViewAdapter(this, jobStoresAndStoresList);
                        jobStoresRecyclerViewAdapter.setHasStableIds(true);
                        recyclerView.setAdapter(jobStoresRecyclerViewAdapter);
                        jobStoresRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    jobStoresRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });

        jobStoresViewModel.getCheckErrors().observe(this, resultStringPair -> {
            jobStoresViewModel.setIsLoading(false);
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                showToast(resultStringPair.second);
            }
        });

        jobStoresViewModel.getIsLoading().observe(this, isLoading -> {
            Log.d(TAG, "setViewModelObservables: isLoading " + isLoading);
            if (isLoading)
                activityRecceJobStoresBinding.progressbarJobStores.getRoot().setVisibility(View.VISIBLE);
            else
                activityRecceJobStoresBinding.progressbarJobStores.getRoot().setVisibility(View.GONE);

        });
    }

    private void init() {

        activityRecceJobStoresBinding.searchViewRecceJobStores.onActionViewExpanded();
        activityRecceJobStoresBinding.searchViewRecceJobStores.clearFocus();

        recceConstraintLayout = activityRecceJobStoresBinding.constraintLayoutRecceJobStores;
        doneConstraintLayout = activityRecceJobStoresBinding.constraintLayoutDoneRecceJobStores;
        revisitConstraintLayout = activityRecceJobStoresBinding.constraintLayoutRevisitRecceJobStores;

        recyclerView = activityRecceJobStoresBinding.recyclerviewRecceJobStores;
        jobStoresViewModel = new ViewModelProvider(this).get(JobStoresViewModel.class);
        filterTypeSpinner = activityRecceJobStoresBinding.spinnerFilterTypeJobStores;

        ArrayAdapter<String> filterAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getFilterTypes());
        filterTypeSpinner.setAdapter(filterAdapter);

        filterTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    filterType = (String) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        activityRecceJobStoresBinding.appbarRecceJobStores.appbarUserText.setText(Utils.getUserName(getApplication()));
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        setRecceAdapter();
    }

    private ArrayList<String> getFilterTypes(){
        ArrayList<String> filters = new ArrayList<>();
        filters.add(storeName);
        filters.add(address);
        filters.add(area);
        return filters;
    }

    private void setOnClickListeners() {
        activityRecceJobStoresBinding.imageBackRecceJobStores.setOnClickListener(v -> onBackPressed());

        activityRecceJobStoresBinding.imageHomeRecceJobStores.setOnClickListener(v -> {
            startActivity(new Intent(JobStoresActivity.this, DashboardActivity.class));
            finish();
        });

        activityRecceJobStoresBinding.searchViewRecceJobStores.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(jobStoresAndStoresList != null && jobStoresRecyclerViewAdapter != null){
                    if(!TextUtils.isEmpty(newText)) {
                        jobStoresRecyclerViewAdapter.setFilter(filter(newText, filterType));
                    } else
                        jobStoresRecyclerViewAdapter.setFilter(jobStoresAndStoresList);
                }
                return true;
            }
        });

//        recceConstraintLayout.setOnClickListener(v -> {
//            //setRecceAdapter();
//            doneConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//            revisitConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//            recceConstraintLayout.setBackgroundColor(getResources().getColor(R.color.white));
//        });
//
//        doneConstraintLayout.setOnClickListener(v -> {
//            //setDoneAdapter();
//            doneConstraintLayout.setBackgroundColor(getResources().getColor(R.color.white));
//            revisitConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//            recceConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//        });
//
//        revisitConstraintLayout.setOnClickListener(v -> {
//            //setRevisitAdapter();
//            doneConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//            revisitConstraintLayout.setBackgroundColor(getResources().getColor(R.color.white));
//            recceConstraintLayout.setBackgroundColor(getResources().getColor(R.color.grey_background));
//        });
    }

//    private void setRevisitAdapter() {
//        if(revisitRecceRecyclerViewAdapter == null){
//            revisitRecceRecyclerViewAdapter = new JobStoresRecyclerViewAdapter(this,3);
//        }
//        recyclerView.setAdapter(revisitRecceRecyclerViewAdapter);
//    }
//
//    private void setDoneAdapter() {
//        if(doneRecceRecyclerViewAdapter == null){
//            doneRecceRecyclerViewAdapter = new JobStoresRecyclerViewAdapter(this,2);
//        }
//        recyclerView.setAdapter(doneRecceRecyclerViewAdapter);
//
//    }
//
//
//    private void setRecceAdapter() {
//        if(recceRecyclerViewAdapter == null){
//            recceRecyclerViewAdapter = new JobStoresRecyclerViewAdapter(this,1);
//        }
//        recyclerView.setAdapter(recceRecyclerViewAdapter);
//
//    }

    private List<JobStoresAndStores> filter(String newText, String filterType) {
        newText = newText.toLowerCase();
        String text = "";
        List<JobStoresAndStores> filteredDataList = new ArrayList<>();
        for (JobStoresAndStores j : jobStoresAndStoresList) {
            if(filterType.equalsIgnoreCase(storeName)) {
                text = j.getStore().getName().toLowerCase();
            } else if(filterType.equalsIgnoreCase(address)) {
                text = j.getStore().getAddress().toLowerCase();
            } else if(filterType.equalsIgnoreCase(area)) {
                text = j.getStore().getArea().toLowerCase();
            }

            if (text.contains(newText)) {
                filteredDataList.add(j);
            }
        }

        return filteredDataList;
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(JobStoresActivity.this, FormActivity.class);
        intent.putExtra(Utils.CLIENT_ID_ROOM, jobStoresAndStoresList.get(position).getClients().getClientId());
        intent.putExtra(Utils.JOBS_ID_ROOM, jobStoresAndStoresList.get(position).getJobStores().getJobId());
        intent.putExtra(Utils.JOB_STORE_ID_ROOM, jobStoresAndStoresList.get(position).getJobStores().getJobStoresId());
        intent.putExtra(Utils.STORE_ID_ROOM, jobStoresAndStoresList.get(position).getStore().getStoreId());
        startActivity(intent);
    }

    @Override
    public void onCallOwnerClicked(int position) {
        String number = jobStoresAndStoresList.get(position).getStore().getMobile();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }

    @Override
    public void onDirectionClicked(int position) {
        String address = jobStoresAndStoresList.get(position).getStore().getAddress();
        Uri mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }
}