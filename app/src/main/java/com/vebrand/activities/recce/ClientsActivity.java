package com.vebrand.activities.recce;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.activities.DashboardActivity;
import com.vebrand.adapters.ClientsRecyclerViewAdapter;
import com.vebrand.databinding.ActivityRecceClientsBinding;
import com.vebrand.model.Clients;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.ClientViewModel;
import com.vebrand.viewmodel.RESULT;

import java.util.List;

public class ClientsActivity extends AppCompatActivity implements ItemClickListener {

    private static final String TAG = ClientsActivity.class.getSimpleName();
    ActivityRecceClientsBinding activityRecceClientsBinding;
    ClientViewModel clientViewModel;

    RecyclerView recyclerView;
    ClientsRecyclerViewAdapter clientsRecyclerViewAdapter;

    List<Clients> clientsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRecceClientsBinding = ActivityRecceClientsBinding.inflate(getLayoutInflater());
        setContentView(activityRecceClientsBinding.getRoot());

        init();
        setOnClickListeners();
        setViewModelObservables();


        clientViewModel.setIsLoading(true);
        clientViewModel.getClientsListFromDatabase();
        if (Utils.isConnect(getApplication()))
            clientViewModel.getClientsFromApi();
        else
            showToast("No Internet Detected!", Toast.LENGTH_LONG);

    }

    private void setViewModelObservables() {
        clientViewModel.getClientsListDatabase().observe(this, clients -> {
            if (clients.size() != 0) {
                clientViewModel.setIsLoading(false);
                if (clientsList == null) {
                    clientsList = clients;
                } else {
                    clientsList.clear();
                    clientsList.addAll(clients);
                }

                if (clientsRecyclerViewAdapter == null) {
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        clientsRecyclerViewAdapter = new ClientsRecyclerViewAdapter(this, clientsList);
                        clientsRecyclerViewAdapter.setHasStableIds(true);
                        recyclerView.setAdapter(clientsRecyclerViewAdapter);
                        clientsRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    clientsRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        });

        clientViewModel.getClientsListApi().observe(this, clients -> {
            if (clients.size() != 0)
                clientViewModel.insertClientListIntoDatabase(clients);
            else {
                clientViewModel.setIsLoading(false);
                showToast("No Clients!");
            }
        });

        clientViewModel.getCheckErrors().observe(this, resultStringPair -> {
            clientViewModel.setIsLoading(false);
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                showToast(resultStringPair.second);
            }
        });

        clientViewModel.getIsLoading().observe(this, isLoading -> {
            if (isLoading)
                activityRecceClientsBinding.progressbarClients.getRoot().setVisibility(View.VISIBLE);
            else activityRecceClientsBinding.progressbarClients.getRoot().setVisibility(View.GONE);

        });

    }

    private void setOnClickListeners() {
        activityRecceClientsBinding.imageHomeRecceClients.setOnClickListener(v -> {
            startActivity(new Intent(ClientsActivity.this, DashboardActivity.class));
            finish();
        });
    }

    private void init() {
        clientViewModel = new ViewModelProvider(this).get(ClientViewModel.class);
        recyclerView = activityRecceClientsBinding.recyclerviewRecceClients;
        activityRecceClientsBinding.appbarRecceClients.appbarUserText.setText(Utils.getUserName(getApplication()));
    }

    @Override
    public void onItemClick(int position) {
        if (clientsList != null) {
            int clientId = clientsList.get(position).getClientId();
            Intent intent = new Intent(ClientsActivity.this, JobsActivity.class);
            intent.putExtra(Utils.CLIENT_ID_ROOM, clientId);
            startActivity(intent);
        }

    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }
}