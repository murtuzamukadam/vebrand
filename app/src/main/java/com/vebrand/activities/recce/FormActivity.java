package com.vebrand.activities.recce;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vebrand.ItemClickListener;
import com.vebrand.activities.DashboardActivity;
import com.vebrand.adapters.FormRecyclerViewAdapter;
import com.vebrand.databinding.ActivityRecceFormBinding;
import com.vebrand.model.FormsWithStatus;
import com.vebrand.model.LocalFormWithStatus;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.FormViewModel;
import com.vebrand.viewmodel.RESULT;

import java.util.ArrayList;
import java.util.List;

public class FormActivity extends AppCompatActivity implements ItemClickListener {
    private static final String TAG = FormActivity.class.getSimpleName();
    ActivityRecceFormBinding activityRecceFormBinding;

    FormViewModel formViewModel;

    List<FormsWithStatus> formList;
    List<LocalFormWithStatus> localFormWithStatusesList;

    RecyclerView recyclerView;
    FormRecyclerViewAdapter formRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRecceFormBinding = ActivityRecceFormBinding.inflate(getLayoutInflater());
        setContentView(activityRecceFormBinding.getRoot());
        init();
        checkIds(getIntent());
        setOnClickListeners();
        setViewModelObservables();
    }

    private void setOnClickListeners() {
        activityRecceFormBinding.fabRecceForm.setOnClickListener(v -> {
            Intent intent = new Intent(FormActivity.this, AddFormActivity.class);
            intent.putExtra(Utils.CLIENT_ID_ROOM, formViewModel.getClientId());
            intent.putExtra(Utils.JOBS_ID_ROOM, formViewModel.getJobId());
            intent.putExtra(Utils.JOB_STORE_ID_ROOM, formViewModel.getJobStoreId());
            intent.putExtra(Utils.STORE_ID_ROOM, formViewModel.getStoreId());
            intent.putExtra(Utils.FORMS_ID_ROOM, -2);
            startActivity(intent);
        });

        activityRecceFormBinding.imageHomeRecceForm.setOnClickListener(v -> {
            startActivity(new Intent(FormActivity.this, DashboardActivity.class));
        });

        activityRecceFormBinding.imageBackRecceForm.setOnClickListener(v -> {
            onBackPressed();
        });
    }

    private void checkIds(Intent intent) {
        int clientId = intent.getIntExtra(Utils.CLIENT_ID_ROOM, -1);
        int jobId = intent.getIntExtra(Utils.JOBS_ID_ROOM, -1);
        int jobStoreId = intent.getIntExtra(Utils.JOB_STORE_ID_ROOM, -1);
        int storeId = intent.getIntExtra(Utils.STORE_ID_ROOM, -1);

        if (clientId != -1 && jobId != -1 && jobStoreId != -1 && storeId != -1) {
            formViewModel.setIsLoading(true);
            formViewModel.setClientId(clientId);
            formViewModel.setJobId(jobId);
            formViewModel.setJobStoreId(jobStoreId);
            formViewModel.setStoreId(storeId);
            formViewModel.getStoreNameFromDatabase();
            formViewModel.getFormsListStatusesFromDatabase();
            formViewModel.getLocalFormsListStatusesFromDatabase();
            if (Utils.isConnect(getApplication())) {
                formViewModel.getFormsFromApi();
            } else showToast("No Internet Detected!", Toast.LENGTH_LONG);
        } else {
            showToast("Something Went Wrong!");
            finish();
        }
    }

    private void setViewModelObservables() {
        formViewModel.getStoreName().observe(this, storeName -> activityRecceFormBinding.textStoreNameRecceForm.setText(storeName));

        formViewModel.getFormListApi().observe(this, forms -> {
            Log.d(TAG, "setViewModelObservables: formsize " + forms.size());
            if (forms.size() != 0)
                formViewModel.insertFormListIntoDatabase(forms);
            else {
                formViewModel.setIsLoading(false);
                showToast("No Forms!");
            }
        });

        formViewModel.getLocalFormListDatabase().observe(this, localForms -> {
            if (localForms.size() != 0) {
                formViewModel.setIsLoading(false);
                if (localFormWithStatusesList == null) {
                    localFormWithStatusesList = localForms;
                } else {
                    localFormWithStatusesList.clear();
                    localFormWithStatusesList.addAll(localForms);
                }

                if (formRecyclerViewAdapter == null) {
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        recyclerView.setNestedScrollingEnabled(true);
                        formRecyclerViewAdapter = new FormRecyclerViewAdapter(this, formList, localFormWithStatusesList);
                        formRecyclerViewAdapter.setHasStableIds(true);
                        recyclerView.setAdapter(formRecyclerViewAdapter);
                        formRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    formRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
            checkStatuses();
        });

        formViewModel.getFormListDatabase().observe(this, forms -> {
            if (forms.size() != 0) {
                formViewModel.setIsLoading(false);
                if (formList == null) {
                    formList = forms;
                } else {
                    formList.clear();
                    formList.addAll(forms);
                }

                if (formRecyclerViewAdapter == null) {
                    if (recyclerView != null) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(this));
                        recyclerView.setNestedScrollingEnabled(true);
                        formRecyclerViewAdapter = new FormRecyclerViewAdapter(this, formList, localFormWithStatusesList);
                        formRecyclerViewAdapter.setHasStableIds(true);
                        recyclerView.setAdapter(formRecyclerViewAdapter);
                        formRecyclerViewAdapter.notifyDataSetChanged();
                    }
                } else {
                    formRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
            checkStatuses();
        });

        formViewModel.getCheckErrors().observe(this, resultStringPair -> {
            formViewModel.setIsLoading(false);
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                showToast(resultStringPair.second);
            }
        });

        formViewModel.getIsLoading().observe(this, isLoading -> {
            if (isLoading)
                activityRecceFormBinding.progressbarForm.getRoot().setVisibility(View.VISIBLE);
            else activityRecceFormBinding.progressbarForm.getRoot().setVisibility(View.GONE);
        });

        formViewModel.getIsUpdateSuccessful().observe(this, isUpdateSuccessful -> {
            Log.d(TAG, "setViewModelObservables: isSuccessful " + isUpdateSuccessful);
            formViewModel.setIsLoading(false);
            if (isUpdateSuccessful) {
                showToast("Uploaded Successfully");
                Intent intent = new Intent(FormActivity.this, FormActivity.class);
                intent.putExtra(Utils.CLIENT_ID_ROOM, formViewModel.getClientId());
                intent.putExtra(Utils.JOBS_ID_ROOM, formViewModel.getJobId());
                intent.putExtra(Utils.JOB_STORE_ID_ROOM, formViewModel.getJobStoreId());
                intent.putExtra(Utils.STORE_ID_ROOM, formViewModel.getStoreId());
                startActivity(intent);
            } else showToast("Form Could Not Be Uploaded!");
        });
    }

    private void checkStatuses() {
        int doneIds = 0;
        for (FormsWithStatus f : formList) {
            if (f.getStatuses().getName().equalsIgnoreCase("done")
                    || f.getStatuses().getName().equalsIgnoreCase("complete")) {
                doneIds++;
            }
        }

        for (LocalFormWithStatus f : localFormWithStatusesList) {
            if (f.getStatuses().getName().equalsIgnoreCase("done")
                    || f.getStatuses().getName().equalsIgnoreCase("complete")) {
                doneIds++;
            }
        }

        if (doneIds == formList.size() + localFormWithStatusesList.size()) {
            activityRecceFormBinding.fabRecceForm.setVisibility(View.GONE);
        } else activityRecceFormBinding.fabRecceForm.setVisibility(View.VISIBLE);
    }

    private void init() {
        formViewModel = new ViewModelProvider(this).get(FormViewModel.class);
        formList = new ArrayList<>();
        localFormWithStatusesList = new ArrayList<>();

        recyclerView = activityRecceFormBinding.recyclerviewRecceForms;

        activityRecceFormBinding.appbarRecceAddForm.appbarUserText.setText(Utils.getUserName(getApplication()));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(true);
        formRecyclerViewAdapter = new FormRecyclerViewAdapter(this, formList, localFormWithStatusesList);
        formRecyclerViewAdapter.setHasStableIds(true);
        recyclerView.setAdapter(formRecyclerViewAdapter);
    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }

    @Override
    public void onItemClick(int position) {
        if (position >= localFormWithStatusesList.size()) {
            int pos = position - localFormWithStatusesList.size();
            Intent intent = new Intent(FormActivity.this, DisplayFormActivity.class);
            intent.putExtra(Utils.CLIENT_ID_ROOM, formList.get(pos).getForm().getClientId());
            intent.putExtra(Utils.JOBS_ID_ROOM, formList.get(pos).getForm().getJobiId());
            intent.putExtra(Utils.JOB_STORE_ID_ROOM, formList.get(pos).getForm().getJobStoreId());
            intent.putExtra(Utils.STORE_ID_ROOM, formList.get(pos).getForm().getStoreId());
            intent.putExtra(Utils.FORMS_ID_ROOM, formList.get(pos).getForm().getFormId());
            startActivity(intent);
        } else {
            Intent intent = new Intent(FormActivity.this, AddFormActivity.class);
            intent.putExtra(Utils.CLIENT_ID_ROOM, localFormWithStatusesList.get(position).getLocalForm().getClientId());
            intent.putExtra(Utils.JOBS_ID_ROOM, localFormWithStatusesList.get(position).getLocalForm().getJobiId());
            intent.putExtra(Utils.JOB_STORE_ID_ROOM, localFormWithStatusesList.get(position).getLocalForm().getJobStoreId());
            intent.putExtra(Utils.STORE_ID_ROOM, localFormWithStatusesList.get(position).getLocalForm().getStoreId());
            intent.putExtra(Utils.FORMS_ID_ROOM, localFormWithStatusesList.get(position).getLocalForm().getFormId());
            startActivity(intent);
        }
    }

    @Override
    public void onDoneClicked(int position) {
        int pos = position - localFormWithStatusesList.size();
        formViewModel.updateFormStatus(String.valueOf(formList.get(pos).getForm().getFormId()));
    }
}