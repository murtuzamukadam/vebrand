package com.vebrand.activities.recce;

import static android.view.View.GONE;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.vebrand.R;
import com.vebrand.activities.DashboardActivity;
import com.vebrand.databinding.ActivityRecceAddFormBinding;
import com.vebrand.model.DisplayLocalForm;
import com.vebrand.model.LocalForm;
import com.vebrand.model.Product;
import com.vebrand.model.Unit;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.AddFormViewModel;
import com.vebrand.viewmodel.RESULT;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.hzw.doodle.DoodleActivity;
import cn.hzw.doodle.DoodleParams;

public class AddFormActivity extends AppCompatActivity {

    private static final String TAG = AddFormActivity.class.getSimpleName();
    private static final int REQ_CODE_DOODLE = 100;
    ActivityRecceAddFormBinding activityRecceAddFormBinding;
    AddFormViewModel addFormViewModel;

    NestedScrollView addFormScrollView;
    Spinner productSpinner, heightSpinner, widthSpinner;
    ImageButton camera, folder;

    DisplayLocalForm displayLocalForm;

    RecyclerView recyclerView;

    AlertDialog pickerDialog;
    View alertView;
    LocalForm localForm;

    String recceImagePath, recce1ImagePath, recce2ImagePath, recce3ImagePath, recce4ImagePath, recceImageCertificate,
            recceImagePathEdited, recce1ImagePathEdited, recce2ImagePathEdited, recce3ImagePathEdited, recce4ImagePathEdited;

    int imageClicked = -1;

    String formId;
    Integer pendingId;

    List<Unit> units = new ArrayList<>();
    List<Product> products = new ArrayList<>();

    ActivityResultLauncher<String> galleryActivityLauncher;
    ActivityResultLauncher<Intent> cameraLauncher;

    boolean isCreate, isImage0Exists, isImage1Exists, isImage2Exists, isImage3Exists, isImage4Exists, isCertificateExists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRecceAddFormBinding = ActivityRecceAddFormBinding.inflate(getLayoutInflater());
        setContentView(activityRecceAddFormBinding.getRoot());
        init();
        checkIds(getIntent());
        setOnClickListeners();
        setOnSelectedListeners();
        setActivityResults();
        setViewModelObservables();
    }

    private void checkIds(Intent intent) {
        int clientId = intent.getIntExtra(Utils.CLIENT_ID_ROOM, -1);
        int jobId = intent.getIntExtra(Utils.JOBS_ID_ROOM, -1);
        int jobStoreId = intent.getIntExtra(Utils.JOB_STORE_ID_ROOM, -1);
        int storeId = intent.getIntExtra(Utils.STORE_ID_ROOM, -1);
        int formId = intent.getIntExtra(Utils.FORMS_ID_ROOM, -1);
        addFormViewModel.getPendingIdFromDatabase("pending");
        Log.d(TAG, "checkIds: " + clientId + jobId + jobStoreId + storeId + formId);


        if (formId == -2) {
            //indicates create new form
            isCreate = true;
            this.formId = String.valueOf(System.currentTimeMillis()).substring(4);
            activityRecceAddFormBinding.imageFolderPhotosAddForm.setVisibility(GONE);
            activityRecceAddFormBinding.imageCameraPhotosAddForm.setVisibility(GONE);
            activityRecceAddFormBinding.imageFolderRecceCertificateAddForm.setVisibility(GONE);
            activityRecceAddFormBinding.imageCameraRecceCertificateAddForm.setVisibility(GONE);

            addFormViewModel.setClientId(clientId);
            addFormViewModel.setJobId(jobId);
            addFormViewModel.setJobStoreId(jobStoreId);
            addFormViewModel.setStoreId(storeId);
            addFormViewModel.getUnitsFromDatabase();
            addFormViewModel.getProductsFromApi();
            addFormViewModel.setFormId(Integer.parseInt(this.formId));
            setPaths();
        } else {
            isCreate = false;
            if (clientId != -1 && jobId != -1 && jobStoreId != -1 && storeId != -1 && formId != -1) {
                activityRecceAddFormBinding.imageFolderPhotosAddForm.setVisibility(GONE);
                activityRecceAddFormBinding.imageCameraPhotosAddForm.setVisibility(GONE);
                activityRecceAddFormBinding.imageFolderRecceCertificateAddForm.setVisibility(GONE);
                activityRecceAddFormBinding.imageCameraRecceCertificateAddForm.setVisibility(GONE);

                addFormViewModel.setClientId(clientId);
                addFormViewModel.setJobId(jobId);
                addFormViewModel.setJobStoreId(jobStoreId);
                addFormViewModel.setStoreId(storeId);
                addFormViewModel.setFormId(formId);
                addFormViewModel.getDisplayLocalFormFromDatabase();
                if (Utils.isConnect(getApplication())) {
                    //formViewModel.getFormsFromApi();
                } else showToast("No Internet Detected!", Toast.LENGTH_LONG);
            } else {
                showToast("Something Went Wrong!");
                finish();
            }
        }
    }

    private void setViewModelObservables() {
        addFormViewModel.getDisplayLocalForm().observe(this, displayLocalForm -> {
            addFormViewModel.setIsLoading(false);
            this.displayLocalForm = displayLocalForm;
            setUI();
            checkImageExists();
        });

        addFormViewModel.getUnitList().observe(this, unitsList -> {
            units.clear();
            units.addAll(unitsList);
            ArrayAdapter<Unit> a = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, units);
            heightSpinner.setAdapter(a);
            if (displayLocalForm != null) {
                heightSpinner.setSelection(units.indexOf(displayLocalForm.getHeight()));
            }

            widthSpinner.setAdapter(a);
            if (displayLocalForm != null) {
                widthSpinner.setSelection(units.indexOf(displayLocalForm.getWidth()));
            }
        });

        addFormViewModel.getProductList().observe(this, productList -> {
            products.clear();
            products.addAll(productList);
            ArrayAdapter<Product> a = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, products);
            productSpinner.setAdapter(a);
            if (displayLocalForm != null) {
                Product product = new Product();
                product.setId(displayLocalForm.getLocalForm().getProductId());
                product.setName(displayLocalForm.getLocalForm().getProductName());
                productSpinner.setSelection(products.indexOf(product));
            }
        });

        addFormViewModel.getPendingId().observe(this, pendingId -> this.pendingId = pendingId);

        addFormViewModel.getImageNumber().observe(this, imageNumber -> {
            imageClicked = imageNumber;
            initiateDoodle();
        });

        addFormViewModel.getIsLocalFormInserted().observe(this, isLocalFormInserted -> {
            addFormViewModel.setIsLoading(false);
            if (isLocalFormInserted) showToast("Local Form Saved");
            else showToast("Something Went Wrong");

            startActivity(new Intent(AddFormActivity.this, DashboardActivity.class));
        });

        addFormViewModel.getIsApiInsertionSuccessful().observe(this, isApiInsertionSuccessful -> {
            Log.d(TAG, "setViewModelObservables: isSuccessful " + isApiInsertionSuccessful);
            addFormViewModel.setIsLoading(false);
            if (isApiInsertionSuccessful) {
                showToast("Uploaded Successfully");
                Intent intent = new Intent(AddFormActivity.this, FormActivity.class);
                intent.putExtra(Utils.CLIENT_ID_ROOM, addFormViewModel.getClientId());
                intent.putExtra(Utils.JOBS_ID_ROOM, addFormViewModel.getJobId());
                intent.putExtra(Utils.JOB_STORE_ID_ROOM, addFormViewModel.getJobStoreId());
                intent.putExtra(Utils.STORE_ID_ROOM, addFormViewModel.getStoreId());
                startActivity(intent);
                finish();
            } else showToast("Form Could Not Be Uploaded!");
        });

        addFormViewModel.getIsLoading().observe(this, isLoading -> {
            if (isLoading)
                activityRecceAddFormBinding.progressbarAddForm.getRoot().setVisibility(View.VISIBLE);
            else activityRecceAddFormBinding.progressbarAddForm.getRoot().setVisibility(GONE);
        });

        addFormViewModel.getCheckErrors().observe(this, resultStringPair -> {
            addFormViewModel.setIsLoading(false);
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                showToast(resultStringPair.second);
            }
        });

    }

    private void checkImageExists() {
        if (displayLocalForm != null) {
            isImage0Exists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getImage0()) && !displayLocalForm.getLocalForm().getImage0().equalsIgnoreCase("null");
            isImage1Exists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getImage1()) && !displayLocalForm.getLocalForm().getImage1().equalsIgnoreCase("null");
            isImage2Exists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getImage2()) && !displayLocalForm.getLocalForm().getImage2().equalsIgnoreCase("null");
            isImage3Exists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getImage3()) && !displayLocalForm.getLocalForm().getImage3().equalsIgnoreCase("null");
            isImage4Exists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getImage4()) && !displayLocalForm.getLocalForm().getImage4().equalsIgnoreCase("null");
            isCertificateExists = !TextUtils.isEmpty(displayLocalForm.getLocalForm().getRecceCertificate()) && !displayLocalForm.getLocalForm().getRecceCertificate().equalsIgnoreCase("null");
        }
    }

    private void setUI() {
        formId = String.valueOf(displayLocalForm.getLocalForm().getFormId());
        setPaths();
        addFormViewModel.getUnitsFromDatabase();
        addFormViewModel.getProductsFromApi();
        activityRecceAddFormBinding.editHeightAddForm.setText(String.valueOf(displayLocalForm.getLocalForm().getHeight()));

        activityRecceAddFormBinding.editWidthAddForm.setText(String.valueOf(displayLocalForm.getLocalForm().getWidth()));

        activityRecceAddFormBinding.editQuantityAddForm.setText(String.valueOf(displayLocalForm.getLocalForm().getQuantity()));

        Unit heightUnit = new Unit();
        heightUnit.setName(displayLocalForm.getHeight().getName());
        heightUnit.setUnitId(displayLocalForm.getHeight().getUnitId());
        units.add(heightUnit);
        ArrayAdapter<Unit> initHeight = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, units);
        heightSpinner.setAdapter(initHeight);

        Unit widthUnit = new Unit();
        widthUnit.setName(displayLocalForm.getWidth().getName());
        widthUnit.setUnitId(displayLocalForm.getWidth().getUnitId());
        units.add(heightUnit);
        ArrayAdapter<Unit> initWidth = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, units);
        widthSpinner.setAdapter(initWidth);

        Product product = new Product();
        product.setId(displayLocalForm.getLocalForm().getProductId());
        product.setName(displayLocalForm.getLocalForm().getProductName());
        products.add(product);
        ArrayAdapter<Product> p = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, products);
        productSpinner.setAdapter(p);
//        Log.d(TAG, "checkIds: " + a.getPosition(displayForm.getHeight().getName()));
//        heightSpinner.setSelection(a.getPosition(displayForm.getHeight().getName()));

        activityRecceAddFormBinding.editRecceRemarkAddForm.setText(displayLocalForm.getLocalForm().getRecceRemark());

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE);

        Glide.with(this).load(displayLocalForm.getLocalForm().getImage0()).apply(options).into(activityRecceAddFormBinding.imageRecceImageAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage1()).apply(options).into(activityRecceAddFormBinding.imageRecce1ImageAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage2()).apply(options).into(activityRecceAddFormBinding.imageRecce2ImageAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage3()).apply(options).into(activityRecceAddFormBinding.imageRecce3ImageAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage4()).apply(options).into(activityRecceAddFormBinding.imageRecce4ImageAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getRecceCertificate()).apply(options).into(activityRecceAddFormBinding.imageRecceCertificateAddForm);

        Glide.with(this).load(displayLocalForm.getLocalForm().getImage0Edited()).apply(options).into(activityRecceAddFormBinding.imageRecceImageEditedAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage1Edited()).apply(options).into(activityRecceAddFormBinding.imageRecce1ImageEditedAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage2Edited()).apply(options).into(activityRecceAddFormBinding.imageRecce2ImageEditedAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage3Edited()).apply(options).into(activityRecceAddFormBinding.imageRecce3ImageEditedAddForm);
        Glide.with(this).load(displayLocalForm.getLocalForm().getImage4Edited()).apply(options).into(activityRecceAddFormBinding.imageRecce4ImageEditedAddForm);

    }

    private void setActivityResults() {
        galleryActivityLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
                result -> {
                    if (result != null) {
                        Log.d(TAG, "onActivityResult: " + result);
                        //addFormViewModel.handleImageResult(result, "formid");
                        if (imageClicked == 0) {
                            addFormViewModel.copyFileUri(result, recceImagePath, 0);
                        } else if (imageClicked == 1) {
                            addFormViewModel.copyFileUri(result, recce1ImagePath, 1);
                        } else if (imageClicked == 2) {
                            addFormViewModel.copyFileUri(result, recce2ImagePath, 2);
                        } else if (imageClicked == 3) {
                            addFormViewModel.copyFileUri(result, recce3ImagePath, 3);
                        } else if (imageClicked == 4) {
                            addFormViewModel.copyFileUri(result, recce4ImagePath, 4);
                        } else if (imageClicked == 5) {
                            addFormViewModel.copyFileUri(result, recceImageCertificate, 5);
                        }

                    } else {
                        Log.d(TAG, "onActivityResult: the result is null for some reason");
                        showToast("Something Went Wrong!");
                    }
                });

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            Log.d(TAG, "setActivityResults: ");
            if (result != null && result.getResultCode() == RESULT_OK) {
                Log.d(TAG, "setActivityResults: not null");
                //Log.d(TAG, "setActivityResults: camera result " + result.getData());
                initiateDoodle();
            }
        });

    }

    private void initiateDoodle() {
        Log.d(TAG, "initiateDoodle: image " + imageClicked);
        if (imageClicked == 0) {
            initiateDoodleActivity(recceImagePath, recceImagePathEdited);
        } else if (imageClicked == 1) {
            initiateDoodleActivity(recce1ImagePath, recce1ImagePathEdited);
        } else if (imageClicked == 2) {
            initiateDoodleActivity(recce2ImagePath, recce2ImagePathEdited);
        } else if (imageClicked == 3) {
            initiateDoodleActivity(recce3ImagePath, recce3ImagePathEdited);
        } else if (imageClicked == 4) {
            initiateDoodleActivity(recce4ImagePath, recce4ImagePathEdited);
        } else if (imageClicked == 5) {
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE);
            isCertificateExists = true;
            Glide.with(this).load(recceImageCertificate).apply(options).into(activityRecceAddFormBinding.imageRecceCertificateAddForm);
        }
    }

    private void setOnClickListeners() {

        activityRecceAddFormBinding.imageBackRecceAddForm.setOnClickListener(v -> onBackPressed());

        activityRecceAddFormBinding.imageHomeRecceAddForm.setOnClickListener(v -> {
            startActivity(new Intent(AddFormActivity.this, DashboardActivity.class));
            finish();
        });

        activityRecceAddFormBinding.imageRecceImageAddForm.setOnClickListener(v -> {
            imageClicked = 0;
            pickerDialog.show();
        });

        activityRecceAddFormBinding.imageRecce1ImageAddForm.setOnClickListener(v -> {
            imageClicked = 1;
            pickerDialog.show();
        });
        activityRecceAddFormBinding.imageRecce2ImageAddForm.setOnClickListener(v -> {
            imageClicked = 2;
            pickerDialog.show();
        });
        activityRecceAddFormBinding.imageRecce3ImageAddForm.setOnClickListener(v -> {
            imageClicked = 3;
            pickerDialog.show();
        });
        activityRecceAddFormBinding.imageRecce4ImageAddForm.setOnClickListener(v -> {
            imageClicked = 4;
            pickerDialog.show();
        });
        activityRecceAddFormBinding.imageRecceCertificateAddForm.setOnClickListener(v -> {
            imageClicked = 5;
            pickerDialog.show();
        });

        folder.setOnClickListener(v -> {
            galleryActivityLauncher.launch("image/*");
            pickerDialog.dismiss();
        });

        camera.setOnClickListener(v -> {
            Log.d(TAG, "setOnClickListeners: camera");
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            String path = "";
            if (imageClicked == 0) {
                path = recceImagePath;
            } else if (imageClicked == 1) {
                path = recce1ImagePath;
            } else if (imageClicked == 2) {
                path = recce2ImagePath;
            } else if (imageClicked == 3) {
                path = recce3ImagePath;
            } else if (imageClicked == 4) {
                path = recce4ImagePath;
            } else if (imageClicked == 5) {
                path = recceImageCertificate;
            }

            Log.d(TAG, "setOnClickListeners: " + imageClicked + " " + path);
            Uri photoURI = FileProvider.getUriForFile(this,
                    "com.vebrand.fileprovider",
                    new File(path));
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            cameraLauncher.launch(cameraIntent);
            pickerDialog.dismiss();
        });

        activityRecceAddFormBinding.buttonSaveAddForm.setOnClickListener(v -> {
            addFormViewModel.setIsLoading(true);
            if (isCreate) {
                insertLocalForm(true);
            } else updateDisplayForm(true);

        });

        activityRecceAddFormBinding.buttonDoneAddForm.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(activityRecceAddFormBinding.editQuantityAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editRecceRemarkAddForm.getText().toString())
                    && !TextUtils.isEmpty(activityRecceAddFormBinding.editHeightAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editWidthAddForm.getText().toString())
                    && isImage0Exists) {
                addFormViewModel.setIsLoading(true);
                if (isCreate)
                    insertLocalForm(false);
                else
                    updateDisplayForm(false);
            } else showToast("Please fill all the Fields! First Image Is Compulsory!");
        });

    }

    private void updateDisplayForm(boolean isSave) {
        if (!TextUtils.isEmpty(activityRecceAddFormBinding.editQuantityAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editRecceRemarkAddForm.getText().toString())
                && !TextUtils.isEmpty(activityRecceAddFormBinding.editHeightAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editWidthAddForm.getText().toString())
                && isImage0Exists) {

            displayLocalForm.getLocalForm().setQuantity(Integer.parseInt(activityRecceAddFormBinding.editQuantityAddForm.getText().toString()));
            displayLocalForm.getLocalForm().setRecceRemark(activityRecceAddFormBinding.editRecceRemarkAddForm.getText().toString());
            displayLocalForm.getLocalForm().setHeight(Integer.parseInt(activityRecceAddFormBinding.editHeightAddForm.getText().toString()));
            displayLocalForm.getLocalForm().setWidth(Integer.parseInt(activityRecceAddFormBinding.editWidthAddForm.getText().toString()));

            displayLocalForm.getLocalForm().setRecceStatusId(pendingId);

            if (isImage0Exists) {
                displayLocalForm.getLocalForm().setImage0(recceImagePath);
                displayLocalForm.getLocalForm().setImage0Edited(recceImagePathEdited);
            } else showToast("First Image Is Compulsory!");
            if (isImage1Exists) {
                displayLocalForm.getLocalForm().setImage1(recce1ImagePath);
                displayLocalForm.getLocalForm().setImage1Edited(recce1ImagePathEdited);
            }
            if (isImage2Exists) {
                displayLocalForm.getLocalForm().setImage2(recce2ImagePath);
                displayLocalForm.getLocalForm().setImage2Edited(recce2ImagePathEdited);
            }
            if (isImage3Exists) {
                displayLocalForm.getLocalForm().setImage3(recce3ImagePath);
                displayLocalForm.getLocalForm().setImage3Edited(recce3ImagePathEdited);
            }
            if (isImage4Exists) {
                displayLocalForm.getLocalForm().setImage4(recce4ImagePath);
                displayLocalForm.getLocalForm().setImage4Edited(recce4ImagePathEdited);
            }
            if (isCertificateExists) {
                displayLocalForm.getLocalForm().setRecceCertificate(recceImageCertificate);
            }
            if (isSave) addFormViewModel.insertLocalForm(displayLocalForm.getLocalForm());
            else {
                if (Utils.isConnect(getApplication()))
                    addFormViewModel.insertFormApi(displayLocalForm, isImage0Exists, isImage1Exists, isImage2Exists, isImage3Exists, isImage4Exists, isCertificateExists);
                else showToast("No Internet Detected!", Toast.LENGTH_LONG);
            }

        } else {
            showToast("Please fill all the Fields! First Image Is Compulsory!");
        }
    }

    private void insertLocalForm(boolean isSave) {
        if (!TextUtils.isEmpty(activityRecceAddFormBinding.editQuantityAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editRecceRemarkAddForm.getText().toString())
                && !TextUtils.isEmpty(activityRecceAddFormBinding.editHeightAddForm.getText().toString()) && !TextUtils.isEmpty(activityRecceAddFormBinding.editWidthAddForm.getText().toString())
                && isImage0Exists) {
            localForm.setFormId(Integer.parseInt(formId));
            localForm.setJobiId(addFormViewModel.getJobId());
            localForm.setJobStoreId(addFormViewModel.getJobStoreId());
            localForm.setStoreId((addFormViewModel.getStoreId()));
            localForm.setClientId(addFormViewModel.getClientId());
            localForm.setRecceStatusId(pendingId);

            localForm.setQuantity(Integer.parseInt(activityRecceAddFormBinding.editQuantityAddForm.getText().toString()));
            localForm.setRecceRemark(activityRecceAddFormBinding.editRecceRemarkAddForm.getText().toString());
            localForm.setHeight(Integer.parseInt(activityRecceAddFormBinding.editHeightAddForm.getText().toString()));
            localForm.setWidth(Integer.parseInt(activityRecceAddFormBinding.editWidthAddForm.getText().toString()));

            if (isImage0Exists) {
                localForm.setImage0(recceImagePath);
                localForm.setImage0Edited(recceImagePathEdited);
            } else showToast("First Image Is Compulsory!");
            if (isImage1Exists) {
                localForm.setImage1(recce1ImagePath);
                localForm.setImage1Edited(recce1ImagePathEdited);
            }
            if (isImage2Exists) {
                localForm.setImage2(recce2ImagePath);
                localForm.setImage2Edited(recce2ImagePathEdited);
            }
            if (isImage3Exists) {
                localForm.setImage3(recce3ImagePath);
                localForm.setImage3Edited(recce3ImagePathEdited);
            }
            if (isImage4Exists) {
                localForm.setImage4(recce4ImagePath);
                localForm.setImage4Edited(recce4ImagePathEdited);
            }
            if (isCertificateExists) {
                localForm.setRecceCertificate(recceImageCertificate);
            }
            Log.d(TAG, "setOnClickListeners:  localForm " + localForm.toString());
            if (isSave) addFormViewModel.insertLocalForm(localForm);
            else {
                if (Utils.isConnect(getApplication()))
                    addFormViewModel.insertFormApi(localForm, isImage0Exists, isImage1Exists, isImage2Exists, isImage3Exists, isImage4Exists, isCertificateExists);
                else showToast("No Internet Detected!", Toast.LENGTH_LONG);
            }
        } else {
            showToast("Please fill all the Fields! First Image Is Compulsory!");
        }
    }


    private void init() {
        addFormScrollView = activityRecceAddFormBinding.scrollViewAddForm;

        productSpinner = activityRecceAddFormBinding.spinnerProductAddForm;
        heightSpinner = activityRecceAddFormBinding.spinnerHeightAddForm;
        widthSpinner = activityRecceAddFormBinding.spinnerWidthAddForm;

        recyclerView = activityRecceAddFormBinding.recyclerviewImagesAddForm;

        alertView = getLayoutInflater().inflate(R.layout.camera_or_image_picker_alert_dialog, activityRecceAddFormBinding.getRoot(), false);
        pickerDialog = new AlertDialog.Builder(this).setView(alertView).create();
        camera = alertView.findViewById(R.id.image_camera_photos_alert_dialog);
        folder = alertView.findViewById(R.id.image_folder_photos_alert_dialog);
        localForm = new LocalForm();

        addFormViewModel = new ViewModelProvider(this).get(AddFormViewModel.class);
        activityRecceAddFormBinding.appbarRecceAddForm.appbarUserText.setText(Utils.getUserName(getApplication()));

    }

    private void setOnSelectedListeners() {
        heightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Unit unit = (Unit) parent.getSelectedItem();
                if (isCreate) {
                    localForm.setHeightId(unit.getUnitId());
                    localForm.setHeightName(unit.getName());
                } else {
                    displayLocalForm.getLocalForm().setHeightId(unit.getUnitId());
                    displayLocalForm.getLocalForm().setHeightName(unit.getName());
                    displayLocalForm.setHeight(unit);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        widthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Unit unit = (Unit) parent.getSelectedItem();
                if (isCreate) {
                    localForm.setWidthId(unit.getUnitId());
                    localForm.setWidthName(unit.getName());
                } else {
                    displayLocalForm.getLocalForm().setWidthId(unit.getUnitId());
                    displayLocalForm.getLocalForm().setWidthName(unit.getName());
                    displayLocalForm.setWidth(unit);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Product product = (Product) parent.getSelectedItem();
                if (isCreate) {
                    localForm.setProductId(product.getId());
                    localForm.setProductName(product.getName());
                } else {
                    displayLocalForm.getLocalForm().setProductId(product.getId());
                    displayLocalForm.getLocalForm().setProductName(product.getName());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setPaths() {
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_image").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_1_image").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_2_image").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_3_image").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_4_image").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_certificate").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_image_edited").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_1_image_edited").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_2_image_edited").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_3_image_edited").mkdirs();
        new File(getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_4_image_edited").mkdirs();

        recceImagePath = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_image" + File.separator + formId + "_recce_image.jpg";
        recce1ImagePath = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_1_image" + File.separator + formId + "_recce_1_image.jpg";
        recce2ImagePath = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_2_image" + File.separator + formId + "_recce_2_image.jpg";
        recce3ImagePath = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_3_image" + File.separator + formId + "_recce_3_image.jpg";
        recce4ImagePath = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_4_image" + File.separator + formId + "_recce_4_image.jpg";
        recceImageCertificate = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_certificate" + File.separator + formId + "_recce_certificate.jpg";

        recceImagePathEdited = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_image_edited" + File.separator + formId + "_recce_image_edited.jpg";
        recce1ImagePathEdited = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_1_image_edited" + File.separator + formId + "_recce_1_image_edited.jpg";
        recce2ImagePathEdited = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_2_image_edited" + File.separator + formId + "_recce_2_image_edited.jpg";
        recce3ImagePathEdited = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_3_image_edited" + File.separator + formId + "_recce_3_image_edited.jpg";
        recce4ImagePathEdited = getExternalFilesDir("images") + File.separator + formId + File.separator + formId + "_recce_4_image_edited" + File.separator + formId + "_recce_4_image_edited.jpg";
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_DOODLE && resultCode == RESULT_OK) {
            String editedImagePath = data.getStringExtra("key_image_path");
            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE);//.dontAnimate().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)
            if (imageClicked == 0) {
                isImage0Exists = true;
                Glide.with(this).load(recceImagePath).apply(options).into(activityRecceAddFormBinding.imageRecceImageAddForm);
                Glide.with(this).load(recceImagePathEdited).apply(options).into(activityRecceAddFormBinding.imageRecceImageEditedAddForm);
            } else if (imageClicked == 1) {
                isImage1Exists = true;
                Glide.with(this).load(recce1ImagePath).apply(options).into(activityRecceAddFormBinding.imageRecce1ImageAddForm);
                Glide.with(this).load(recce1ImagePathEdited).apply(options).into(activityRecceAddFormBinding.imageRecce1ImageEditedAddForm);
            } else if (imageClicked == 2) {
                isImage2Exists = true;
                Glide.with(this).load(recce2ImagePath).apply(options).into(activityRecceAddFormBinding.imageRecce2ImageAddForm);
                Glide.with(this).load(recce2ImagePathEdited).apply(options).into(activityRecceAddFormBinding.imageRecce2ImageEditedAddForm);
            } else if (imageClicked == 3) {
                isImage3Exists = true;
                Glide.with(this).load(recce3ImagePath).apply(options).into(activityRecceAddFormBinding.imageRecce3ImageAddForm);
                Glide.with(this).load(recce3ImagePathEdited).apply(options).into(activityRecceAddFormBinding.imageRecce3ImageEditedAddForm);
            } else if (imageClicked == 4) {
                isImage4Exists = true;
                Glide.with(this).load(recce4ImagePath).apply(options).into(activityRecceAddFormBinding.imageRecce4ImageAddForm);
                Glide.with(this).load(recce4ImagePathEdited).apply(options).into(activityRecceAddFormBinding.imageRecce4ImageEditedAddForm);
            }
            imageClicked = -1;
        }
    }

    private void initiateDoodleActivity(String inputPath, String outputPath) {
        DoodleParams params = new DoodleParams(); // 涂鸦参数
        // the file path of image
        Log.d(TAG, "initiateDoodleActivity: inputPath " + inputPath + " outputPath " + outputPath);
        if (inputPath != null && outputPath != null && !inputPath.isEmpty() && !outputPath.isEmpty()) {
            params.mImagePath = inputPath;
            params.mSavePath = outputPath;
            params.mPaintUnitSize = 5;
            DoodleActivity.startActivityForResult(AddFormActivity.this, params, REQ_CODE_DOODLE);
        } else showToast("Something went Wrong");

    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }

    void test(View v) {
        v.animate().rotationBy(180);
    }
}