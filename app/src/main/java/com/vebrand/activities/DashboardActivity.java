package com.vebrand.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.vebrand.activities.recce.ClientsActivity;
import com.vebrand.databinding.ActivityDashboardBinding;
import com.vebrand.viewmodel.DashboardViewModel;
import com.vebrand.viewmodel.RESULT;

public class DashboardActivity extends AppCompatActivity {

    private static final String TAG = DashboardActivity.class.getSimpleName();
    ActivityDashboardBinding activityDashboardBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDashboardBinding = ActivityDashboardBinding.inflate(getLayoutInflater());

        DashboardViewModel dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        dashboardViewModel.getUser();

        dashboardViewModel.getUserMutableLiveData().observe(this, booleanUserPair -> {
            if (booleanUserPair.first) {
                setContentView(activityDashboardBinding.getRoot());
                activityDashboardBinding.appbarMain.appbarUserText.setText(booleanUserPair.second.getFirstName());
                showToast(booleanUserPair.second.getToken());
                dashboardViewModel.getMasters();
            } else {
                startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                finish();
            }
        });

        dashboardViewModel.getCheckErrors().observe(this, resultStringPair -> {
            if (resultStringPair.first == RESULT.ERROR) {
                showToast("Something Went Wrong! database");
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
            }
        });

        //startActivity(new Intent(this,LoginActivity.class));


        activityDashboardBinding.cardviewRecce.setOnClickListener(v -> {
            startActivity(new Intent(DashboardActivity.this, ClientsActivity.class));
        });

    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }
}