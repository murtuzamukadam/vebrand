package com.vebrand.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.color.MaterialColors;
import com.vebrand.R;
import com.vebrand.databinding.ActivityLoginBinding;
import com.vebrand.model.Team;
import com.vebrand.utils.Utils;
import com.vebrand.viewmodel.LoginViewModel;
import com.vebrand.viewmodel.RESULT;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    ActivityLoginBinding activityLoginBinding;
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(activityLoginBinding.getRoot());
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        setViewModelObservers();

        activityLoginBinding.buttonLoginLogin.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(activityLoginBinding.edittextUsernameLogin.getEditText().getText().toString()) && !TextUtils.isEmpty(activityLoginBinding.edittextPasswordLogin.getEditText().getText().toString())) {
                if (Utils.isConnect(getApplication())) {
                    loginViewModel.requestOTP(activityLoginBinding.edittextUsernameLogin.getEditText().getText().toString());
                } else showToast("No Internet Detected");
            } else showToast("Please fill Username and Password!");
        });


    }

    private void setViewModelObservers() {

        loginViewModel.getRequestOTPResponse().observe(this, user -> {
            if (user != null) {
                loginViewModel.checkLogin(user.getLogin(), activityLoginBinding.edittextPasswordLogin.getEditText().getText().toString());
            }
        });

        loginViewModel.getCheckLoginResponse().observe(this, user -> {
            if (user != null) {
                Team team = user.getTeam();
                user.setFirstName(team.getFirstName());
                user.setLastName(team.getLastName());
                user.setEmail(team.getEmail());
                user.setMobile(team.getMobile());
                Utils.insertToken(getApplication(), user.getToken());
                Utils.setUserName(getApplication(),user.getFirstName());
                loginViewModel.insertNewUser(user);
            }
        });

        loginViewModel.getIsUserInserted().observe(this, isUserInserted -> {
            if (isUserInserted) {
                showToast("Login Successful");
                startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                finish();
            }
        });

        loginViewModel.getCheckErrors().observe(this, resultStringPair -> {
            if (resultStringPair.first == RESULT.ERROR) {
                Log.d(TAG, "onChanged: error " + resultStringPair.second);
                loginViewModel.setIsLoading(false);
                showToast("Something went wrong! Please Check Your Internet");
            } else if (resultStringPair.first == RESULT.UNSUCCESSFUL) {
                loginViewModel.setIsLoading(false);
                showToast(resultStringPair.second);
            }
        });

        loginViewModel.getIsLoading().observe(this, isLoading -> {
            if (isLoading) {
                activityLoginBinding.buttonLoginLogin.setBackgroundColor(getResources().getColor(R.color.grey_background));
                activityLoginBinding.progressbarLogin.setVisibility(View.VISIBLE);
                //activityLoginBinding.contsraintProgressLogin.setVisibility(View.VISIBLE);
                activityLoginBinding.buttonLoginLogin.setEnabled(false);
            } else {
                activityLoginBinding.buttonLoginLogin.setBackgroundColor(MaterialColors.getColor(this, R.attr.colorPrimaryVariant, getResources().getColor(R.color.app_blue_dark)));
                activityLoginBinding.progressbarLogin.setVisibility(View.GONE);
                activityLoginBinding.buttonLoginLogin.setEnabled(true);
                // activityLoginBinding.contsraintProgressLogin.setVisibility(View.GONE);
            }

        });
    }

    void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    void showToast(String message, int length) {
        Toast.makeText(this, message, length).show();
    }
}